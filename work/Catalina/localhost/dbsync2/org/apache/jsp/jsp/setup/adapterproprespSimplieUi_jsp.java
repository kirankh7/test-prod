/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: Apache Tomcat/7.0.22
 * Generated at: 2012-04-25 17:12:19 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp.jsp.setup;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import com.avankia.appmashups.web.DBSyncProfileDefinitionModel;
import com.avankia.appmashups.engine.conversion.setup.AdapterSetup;
import java.util.*;
import java.text.*;
import com.avankia.appmashups.web.EncryptDecryptUtil;
import com.avankia.appmashups.engine.conversion.adapters.AdapterProperties;
import com.avankia.appmashups.web.SimpleUI;
import com.avankia.appmashups.engine.conversion.IConversionAdapter;
import com.avankia.appmashups.engine.conversion.IReader;
import com.avankia.appmashups.engine.conversion.IValidate;

public final class adapterproprespSimplieUi_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
 DBSyncProfileDefinitionModel model = (DBSyncProfileDefinitionModel)request.getAttribute("model");
        Properties selectedAdapterProps = model.getAdapterProperties();
        

      out.write("\r\n");
      out.write("<style type=\"text/css\">\r\n");
      out.write("#box-table-b td {\r\n");
      out.write("padding:5px !important;\r\n");
      out.write("}\r\n");
      out.write("</style>\r\n");
      out.write("<div id='bp_property_1'>\r\n");
      out.write("    <form name=\"propertyform\"  id=\"property_adaptor_form\" action=\"dbsyncprofiledefinition.m\" method=\"post\"  >\r\n");
      out.write("\t\t<div class=\"edit_adaptor\">Edit Properties  </div>\r\n");
      out.write("        <div class=\"cf_bp\">\r\n");
      out.write("            <table align=\"left\" style=\"background-color:white;\" id=\"box-table-b\" border=\"1\">\r\n");
      out.write("\t\t\t\t<tr>\r\n");
      out.write("\t\t\t\t\t<span class=\"display_results\"></span>\r\n");
      out.write("\t\t\t\t</tr>\r\n");
      out.write("                <tr class=\"tr_property\">\r\n");
      out.write("                    <td >Connection Properties </td>\r\n");
      out.write("                </tr>\r\n");
      out.write("                <tr>\r\n");
      out.write("                    <td>\r\n");
      out.write("                        ");

                                                if (selectedAdapterProps != null && model.getSelectedAdapter() != null)
                                                {
                        
      out.write("\t\t\t\tName:&nbsp;");
      out.print( model.getAdapterMetaData().getProperty(DBSyncProfileDefinitionModel.ADAPTER_NAME) );
      out.write("<p/>\r\n");
      out.write("\t\t\t\t\t\tType:&nbsp;");
      out.print( model.getAdapterMetaData().getProperty(DBSyncProfileDefinitionModel.ADAPTER_DESCRIPTION) );
      out.write("<p/>\r\n");
      out.write("\t\t\t\t\t\t");

						Class cls = AdapterSetup.getAdapterClass(model.getAdapterMetaData().getProperty(DBSyncProfileDefinitionModel.ADAPTER_TYPE));
						//Class cls = Class.forName(model.getAdapterMetaData().getProperty(DBSyncProfileDefinitionModel.ADAPTER_TYPE));
						//IConversionAdapter adapter = (IConversionAdapter) cls.newInstance();
						IConversionAdapter adapter = AdapterSetup.getConversionAdapter(model.getAdapterMetaData().getProperty(DBSyncProfileDefinitionModel.ADAPTER_TYPE));
						IReader reader = adapter.getReader();
						AdapterProperties adapterProperties = (AdapterProperties)cls.getAnnotation(AdapterProperties.class);
						AdapterProperties.Property[] props = adapterProperties.adapterProps();						
						//org.jdom.Element e = new org.jdom.Element("adapter");					
						SimpleUI so = new SimpleUI();						
						Map<String,SimpleUI> annotationObj = new HashMap<String,SimpleUI>();							
						annotationObj = so.loadProperties(props);
						
						
						
      out.write("\r\n");
      out.write("\t\t\t\t\t\t\r\n");
      out.write("                        <table  cellpadding=\"2\" cellspacing=\"0\"  id=\"blueborderTD\" >\r\n");
      out.write("                           <script language=\"javascript\" type=\"text/javascript\">\r\n");
      out.write("                                helpurl=\"selectedAdapter\";\r\n");
      out.write("                            </script>\r\n");
      out.write("                            ");

                    ArrayList<String> adapterPropsNames = model.getPropertyList();
                           
                                    if (selectedAdapterProps.size() > 0)
                                    {                                    	                                    	
                                            for (String propName : adapterPropsNames)
                                            {
                                                    String propValue = selectedAdapterProps.getProperty(propName);
                                                    SimpleUI annotationObject = new SimpleUI();
                                                    annotationObject= annotationObj.get(propName);                                                                                                      
                            
      out.write("\r\n");
      out.write("                            <tr class=\"tr_odd\">\r\n");
      out.write("                           \t <td onmouseover=\"$(this).find('.tool_tip').show()\"  onmouseout=\"$(this).find('.tool_tip').hide()\">\r\n");
      out.write("\t\t\t\t\t\t\t\t\r\n");
      out.write("\t\t\t\t\t\t\t\t<a href=\"#\" > <img alt='' src='images/help.png'/></a>\r\n");
      out.write("                              \t<div class=\"tool_tip\" style=\"display: none\"   onmouseover=\"$(this).show()\"  onmouseout=\"$(this).hide()\">");
      out.print(annotationObject.getPropDesc());
      out.write("\r\n");
      out.write("                                </div>       \r\n");
      out.write("\t\t\t\t\t\t\t\t</td>\r\n");
      out.write("                                <td class=\"td_noborder\" onmouseover=\"$(this).find('.tool_tip2').show()\"  onmouseout=\"$(this).find('.tool_tip2').hide()\" > \r\n");
      out.write("                                ");
      out.print( propName );
      out.write(":\r\n");
      out.write("                               \t");

                               	if(annotationObject.isRequired()==true){
                            	
      out.write("\r\n");
      out.write("                            \t<span class=\"location\">\r\n");
      out.write("                            \t\t<a href=\"#\" > <img alt='' src='images/bullet_red.png'/></a>\r\n");
      out.write("                            \t\t</span>\r\n");
      out.write("                                     <div class=\"tool_tip2\" style=\"display: none\"   onmouseover=\"$(this).show()\"  onmouseout=\"$(this).hide()\">\r\n");
      out.write("                                    \"Required\"\r\n");
      out.write("                                      </div> \r\n");
      out.write("                            \t");
   
                            	 }
      out.write("\r\n");
      out.write("                                \r\n");
      out.write("                                </td>\r\n");
      out.write("\t\t\t\t\t\t\t\t\r\n");
      out.write("                                <td style=\"padding:0 8px 4px 1px;\" >\r\n");
      out.write("                                    ");

                                            if (propValue.startsWith("URL:")){
                                                    // this is a URL link to download
                                                    String url = propValue.substring(propValue.indexOf("URL:")+4);
                                                    url = MessageFormat.format(url,new Object[]{model.getProfileName()});
                                    
      out.write("\r\n");
      out.write("                                    <a href=\"");
      out.print(url);
      out.write("\" target=\"_new\" id=\"blueborderTD\">Configuration Link</a>\r\n");
      out.write("                                    \r\n");
      out.write("                                    ");

                                            }
                                    
                                    
                                           else {
                                    
      out.write("\r\n");
      out.write("                                    <input size=\"82%\"  type=\"");
      out.print( (propName.toLowerCase().indexOf("password") >= 0) ? "password" : "text" );
      out.write("\" name=\"prop_");
      out.print( propName );
      out.write("\" id=\"prop_");
      out.print( propName );
      out.write("\" value=\"");
      out.print((propName.equals("password")) ? (propValue.equals("")?"":EncryptDecryptUtil.doEncrypt(propValue)) : propValue);
      out.write("\"/>\r\n");
      out.write("                                   \t\r\n");
      out.write("                                    ");

                                            }
                                    
      out.write("\r\n");
      out.write("\r\n");
      out.write("                                </td>\r\n");
      out.write("\t\t\t\t\t\t\t\t \r\n");
      out.write("                            </tr>\r\n");
      out.write("                            ");

                                    }
                            
      out.write("\r\n");
      out.write("\r\n");
      out.write("                            <tr>\r\n");
      out.write("                            \t<td id=\"blueborderTD\" class=\"td_noborder\">&nbsp;</td>\r\n");
      out.write("                                <td id=\"blueborderTD\" class=\"td_noborder\">&nbsp;</td>\r\n");
      out.write("                                <td align=\"right\" id=\"blueborderTD\" class=\"td_noborder\" >\r\n");
      out.write("                                    ");

                                    String adapterNam = model.getProfileName();
                                    String selectedAdapt = model.getSelectedAdapter();
                                    String currentAdapterType = model.getCurrentAdapterType();
                                    
      out.write("\r\n");
      out.write("\r\n");
      out.write("                                    <input  type=\"hidden\" name=\"selectedAdapter\" value=\"");
      out.print(selectedAdapt);
      out.write("\"/>\r\n");
      out.write("                                    <input  type=\"hidden\" name=\"selectedAdapterType\" value=\"");
      out.print(currentAdapterType);
      out.write("\"/>\r\n");
      out.write("                                    ");
if(reader instanceof IValidate){
      out.write("\r\n");
      out.write("                                    <input  type=\"button\" value=\"Save Properties\" onclick=\"save_property('");
      out.print(selectedAdapt);
      out.write('\'');
      out.write(',');
      out.write('\'');
      out.print(currentAdapterType);
      out.write("','true')\" />\r\n");
      out.write("                                    ");
 }else{
      out.write("\r\n");
      out.write("                                     <input  type=\"button\" value=\"Save Properties\" onclick=\"save_property('");
      out.print(selectedAdapt);
      out.write('\'');
      out.write(',');
      out.write('\'');
      out.print(currentAdapterType);
      out.write("','false')\" />\r\n");
      out.write("                                      ");
 }
      out.write(" \r\n");
      out.write("                              \t\t");
if(reader instanceof IValidate){
      out.write("                                \r\n");
      out.write("                                   <a href=\"#\" class=\"run\" onclick=\"save_property('");
      out.print(selectedAdapt);
      out.write('\'');
      out.write(',');
      out.write('\'');
      out.print(currentAdapterType);
      out.write("','true')\">Validate</a>\r\n");
      out.write("                                    ");
 }
      out.write(" \r\n");
      out.write("                                      \r\n");
      out.write("                                   \r\n");
      out.write("                                    \r\n");
      out.write("                            </tr>\r\n");
      out.write("                            ");

                            }
                            else
                            {
                                     String adapterNam = model.getProfileName();
                                            String selectedAdapt = model.getSelectedAdapter();
                            
      out.write("\r\n");
      out.write("                            <tr class=\"tr_header\" >\r\n");
      out.write("                                <td  id=\"blueborderTD\" width=\"82%\" colspan=\"2\">\r\n");
      out.write("                                    <em>This adapter type does not have any properties</em>\r\n");
      out.write("                                  ");
      out.write("                                   \r\n");
      out.write("                                </td>\r\n");
      out.write("                            </tr>\r\n");
      out.write("                            ");

                    	}
                            
      out.write("</table>                        \r\n");
      out.write("                     </td>\r\n");
      out.write("                </tr>");


    }
    else
    {
                            
      out.write("\r\n");
      out.write("                <tr><td>\r\n");
      out.write("                        <p>\r\n");
      out.write("                            <em>Currently no adapters are available. Create a new adapter from the above section.</em>\r\n");
      out.write("                        </p>\r\n");
      out.write("                    </td>\r\n");
      out.write("                </tr>\r\n");
      out.write("                 </td>\r\n");
      out.write("                </tr>\r\n");
      out.write("              \r\n");
      out.write("                ");

        }
                
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("               \r\n");
      out.write("            </table>\r\n");
      out.write("        </div> <!--cf_bp -->\r\n");
      out.write("\r\n");
      out.write("    </form>\r\n");
      out.write("</div> <!--bp_property_1 -->\r\n");
      out.write("<div class=\"clear_float\"></div>\r\n");
      out.write("<div class=\"clear_float\"></div>\r\n");
      out.write("\t<div class='notes3'>\r\n");
      out.write("\t\t<div style='font-size: 12px; color: #525252;'><b>Hint:</b>\r\n");
      out.write("\t\t</div><br/><img alt='mandatory' src='images/bullet_red.png'/> Required Fields,<BR/>\r\n");
      out.write("\t\t<img alt='help' src='images/help.png'/> Help.\r\n");
      out.write("\t</div>\r\n");
      out.write("\t\r\n");
      out.write("<div class=\"clear_float\"></div>");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
