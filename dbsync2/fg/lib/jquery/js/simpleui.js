function message_box(message){	
                $(".message").empty().show().text(message);
                setTimeout('$(".message").hide("slow").empty()','2000');
            }
            function message_box2(message){
                $(".displaystatus").empty().show().text(message);
                // setTimeout('$(".message").hide("slow").empty()','2000');
            }
			function message_box_disp(classname,message){
				$('<span class="err_msg_parag" style="display:inline-block;width:100%;text-align:center; color:red">'+message+'</span>').insertBefore(classname);
				 setTimeout('$(".err_msg_parag").hide("slow").empty()','2000');
			}
			function validate_message_box(message){			
                $(".display_results").empty().show().text(message);
              //  $('.display_results').append("this text was appended");
                
             //    setTimeout('$(".display_results").hide("slow").empty()','4000');
             
            }
            
            
            function edit_adaptor_prop(param,currentAdapterType){
            	//alert(currentAdapterType);
            	var adapterTypeUI=new Array();
                 adapterTypeUI = currentAdapterType.split('.');
               if(adapterTypeUI.length > 0){
                 adapterTypeUI = adapterTypeUI[adapterTypeUI.length -1];
               }else{
               	adapterTypeUI = '';
               }
                var mytime= "&ms="+new Date().getTime();
                $.ajax({
                    url:'dbsyncprofiledefinition.m?command=simpleuipropertysection&profileName='+profileName+'&selectedAdapter='+param+mytime,
                    success:function(data){
                    	
                        $('#tabs-1').html(data);                      
                        
                        if(adapterTypeUI=='QuickBooksAdapter2'){	
                        											
						$('#tabs-1').append("<div class='notes2'><div style='font-size: 12px; color: #525252;'><b>Configuring Quickbooks Web Connector with DBSync:</b></div>" +
								"<BR/>1.Open your company quickbooks file logging as administrator." +
								"<BR/>2.Click on the configuration link above." +
								"<br/>3.QuickBooks Web Connector(QWC) would open up on your desktop along with Authenticating Screens. Complete the process of authentication."+
								"<BR/>4.Enter the password of your DBSync Application in QWC Password section and save it." +
								"<BR/>5.Click on update selected button in QWC, to run the Sync." +
								"<BR/>6.For more details click " +
								"<a href='http://www.appmashups.com/dbsyncwk/index.php?title=Dbsync02:Adapter_QuickBooks' target='_blank'>here</a></div>");
								
						}
                       
                    }
                });

				
                                   
            }

            function save_property(selectdAdapter,currentAdapterType,hasValidate){
			   //alert(currentAdapterType+"----------"+selectdAdapter);			
                var mytime= "&ms="+new Date().getTime();
                var adapterType=new Array();
                 adapterType = currentAdapterType.split('.');
               if(adapterType.length > 0){
                 adapterType = adapterType[adapterType.length -1];
               }else{
               	adapterType = '';
               }
				//$.blockUI();
                $.ajax({
                    type: "GET",
                    url: $("#property_adaptor_form").attr('action'),
                    data:"command=saveAdapterProperties&profileName="+profileName+"&"+$("#property_adaptor_form").serialize()+mytime,
                    cache:false,
                    success:function(data){                                        
                        //alert("Saved Successfully");                       
                          validate_message_box('Saved Successfully');
						if(hasValidate=='true'){						 	 
							validateConnectionProperties(selectdAdapter);							
			             }			              	
                    },
                    error:function(xhr,status,error){
                        if(xhr.status==200){
                            // message_box('List Adaptor','Session expired. Please login once again');
                            alert("Session expired. Please login once again");
                        }else{
                            //  message_box('Load PDL', status+"<br>"+xhr.status+' : '+xhr.statusText);
                            alert("Error saving");
                        }
                    }
                });
                
               	
            }

            function displayStates(selectedPDL,pdlDir){
                var mytime= "&ms="+new Date().getTime();
                $.ajax({
                    type: "POST",
                    url: "simpleui.m",
                    data:"command=displayStates&profileName="+profileName+"&dirPath="+pdlDir+"&selectedPdlname="+selectedPDL+mytime,
                    cache:false,
                    success:function(data){

                        document.getElementById("state_list").innerHTML= data;
                        $("#state_list").show();
                    },
                    error:function(xhr,status,error){
                        if(xhr.status==200){
                            alert("Session expired. Please login once again");
                        }else{
                            alert("Error Opening");
                        }
                    }
                });
            }
			
            function setActiveStatus(chkbox,stateId,selectedpdlName,profileName,pdlDir){
                //alert(stateId+"**"+selectedpdlName+"**"+profileName+"**"+pdlDir);
                var mytime= "&ms="+new Date().getTime();
                var chkSetting = (chkbox.checked) ? "true" : "false";
                $.ajax({
                    type: "POST",
                    url: "simpleui.m",
                    data:"command=setActiveStatus&profileName="+profileName+"&dirPath="+pdlDir+"&selectedPdlname="+selectedpdlName+"&activeStateId="+stateId+"&activeStatus="+chkSetting+mytime,
                    cache:false,
                    success:function(data){
                        if(chkSetting=='true'){
                            message_box("!This step will be executed.");
                            // message_box2("Enabled");
                            $(chkbox).next('.displaystatus').html('<img src="images/confirm16.gif" alt="" /> <b style="clear:both;float:none;"/>');

                        }
                        if(chkSetting=='false'){
                            message_box("!This step will not be executed.");                            
                            $(chkbox).next('.displaystatus').html('<img src="images/cross.png" alt="" /><b style="clear:both;float:none;"/>');

                        }
                      
                    },
                    error:function(xhr,status,error){
                        if(xhr.status==200){
                            alert("Session expired. Please login once again");
                        }else{
                            alert("Error Opening");
                        }
                    }
                });

            }

            function setValidateRow(vld_chkbox,mapfileName,pdlDir){
                var vld_chkbox_setting = (vld_chkbox.checked) ? "true" : "false";
                var mapFilePt= pdlDir+"/"+mapfileName;
                // alert(mapfileName+"------------"+pdlDir);
                var mytime= "&ms="+new Date().getTime();
                $.ajax({
                    type: "POST",
                    url: "simpleui.m",
                    data:"command=setValidateRowStatus&profileName="+profileName+"&dirPath="+mapFilePt+"&selectedPdlname="+mapfileName+"&activeStatus="+vld_chkbox_setting+mytime,
                    cache:false,
                    success:function(data){
                        if(vld_chkbox_setting=='true'){
                            message_box("!This task will be executed.");
                            $(vld_chkbox).next('.enable_text').html('<img src="images/confirm16.gif" alt="" /> <b style="clear:both;float:none;"/>');
                            // message_box2("Enabled");
                        }
                        if(vld_chkbox_setting=='false'){
                            message_box("!This task will not be executed.");
                            // message_box2(" ");
                            $(vld_chkbox).next('.enable_text').html('<img src="images/cross.png" alt=""/><b style="clear:both;float:none;"/>');

                        }
                    },
                    error:function(xhr,status,error){
                        if(xhr.status==200){
                            alert("Session expired. Please login once again");
                        }else{
                            alert("Error Opening");
                        }
                    }
                });
            }

            function runmsg(selectedpdlName){
                run_pdl(selectedpdlName);
              //  alert('Your request has been started. You will receive an email once it is completed.');
            }

         /*   function run_pdl(selectedpdlName){

                var run_url='runpdl.m?profileName='+profileName+'&processFileName='+selectedpdlName+'&command=start';
                window.open(run_url);
            }*/
            function run_pdl(selected_pdl_name){
	
		    var run_url='runpdl.m?profileName='+profileName+'&processFileName='+selected_pdl_name+'&command=start';
		     var mytime= "&ms="+new Date().getTime();
		  //  window.open(run_url);
		    $.ajax({
		                type: "GET",
		                url: run_url+mytime,               
		                cache:false,
		                success:function(data){                                   
		                 // alert('Your request has been started. You will receive an email once it is completed.');
		                 alert('Your request has been submitted. Please wait till the process gets completed.');
		                },
		                error:function(xhr,status,error){                  
		                    alert('Run Status','Failed to run.');                    
		                }
		            });
		}
		function validatepdl(selected_pdl_name,profileName){
			  var validate_url='validateconnection.m?currentPDLName='+selected_pdl_name+'&profileName='+profileName+'&command=validatePDL';
		      var mytime="&ms="+new Date().getTime();		        
		      window.open(validate_url+mytime,'Validate Process','width=1000,height=500,menubar=no,status=yes,location=no,toolbar=no,scrollbars=yes');
		      alert('Your request has been started. You will receive an email once it is completed.');
		 
		}
		
	/*	function loadValidatePage(selected_pdl_name,profileName){ 	
			 var validate_url='validateconnection.m?currentPDLName='+selected_pdl_name+'&profileName='+profileName+'&command=loadValidateProperty';
			  var mytime="&ms="+new Date().getTime();		
		}
	*/
            function saveAdvancedProperties()
            {
               
              //  var emailId = document.advanced_property_form.emailId.value;
               var emailId ='';
                var logLevel = document.advanced_property_form.logLevel.value;
                var mytime= "&ms="+new Date().getTime();
                $.ajax({
                    type: "GET",
                    url: 'dbsyncprofiledefinition.m',
                    data: "command=saveAdvancedProperties&profileName="+profileName+"&emailId="+emailId+"&logLevel="+logLevel+mytime,
                    cache:false,
                    success: function(msg){
                        //  message_box('Properties','Successfully saved.');
                        //message_box('Successfully saved.');
                        message_box_disp('.advnncd_prop','Successfully saved.');
                     //   alert('Successfully saved.');  
                        
                       // validate_message_box('Successfully saved.');                     
                    }

                });
            }
			String.prototype.startsWith = function(str)
				{return (this.match("^"+str)==str)}
				
            function validateConnectionProperties(selectedAdapter){
              //  var profileName = '<%=model.getProfileName()%>';  
                $.blockUI();           
                 var mytime= "&ms="+new Date().getTime();
                $.ajax({
                    type: "GET",
                    url: 'validateconnection.m',
                    data: "command=validateConnection&profileName="+profileName+"&selectedAdapter="+selectedAdapter+mytime,
                    success: function(msg){
						$.unblockUI();
                        msg = $.trim(msg);
                        if(msg =='ConnectionSuccess'){
                            // message_box('Connection Status','Connection Settings are Valid');
                           connection_success = 'true'; 
                            validate_flag='true'; 
                           // alert('Connection Settings are Valid');
                           validate_message_box('Connection settings are Valid');
                           
                        }
                         if(msg.startsWith('ConnectionFail')){
                            // message_box('Connection Status','Connection Settings are Valid');
                           //  msg=msg.substring(15,msg.length);
                            connection_success = 'false';
                            validate_flag='false'; 
                            //alert('Connection Settings are Invalid');
                             validate_message_box('Connection settings are Invalid. ');
                             validate_message_box_hint(msg.substring(14));
                        }
                         if(msg=='Invalid'){
                            // message_box('Connection Status','Connection Settings are Invalid.');
                            connection_success = 'false';
                            validate_flag='false'; 
                           // alert('Connection Settings are Invalid');
                             validate_message_box('Connection settings are Invalid');
                             //validate_message_box_hint();
                             validate_message_box_hint(msg.substring(7));
                        }

                        //$(this).html('');
                    }
                });
            }

            $(document).ready(function(){
                $("#tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
                $("#tabs li").removeClass('ui-corner-top').addClass('ui-corner-left');
                $('.toScroll').jScrollPane({
                    showArrows : true,
                    animateTo:true
                });

         /*       $("#next").click(function(){
                    var $tabs=$("#tabs").tabs();
                    clickNext($tabs.tabs('option', 'selected'));
                });

                $("#previous").click(function(){
                    var $tabs=$("#tabs").tabs();
                    clickPrevious($tabs.tabs('option', 'selected'))
                });
                //select the first tab
                $('#tabs ul li:first a').click();
                **/
                
                
            /*    $("#wait_dialog").ajaxStart(function() {
			        $.blockUI({
			            css: {
			                border: 'none',
			                padding: '15px',
			                backgroundColor: '#000',
			                '-webkit-border-radius': '10px',
			                '-moz-border-radius': '10px',
			                opacity: .5,
			                color: '#fff'
			            },
			            message: 'Please Wait...'//$('#wait_dialog')
			        });
			    });
			    $("#wait_dialog").ajaxSuccess(function() {
			        $.unblockUI();
			    });
			    $("#wait_dialog").ajaxError(function() {
			        $.unblockUI();
			    });
                */
            });

            function clickNext(index){
            	//alert(index+"--index");
                var length  =   $("#tabs").tabs('length');
                var next    =   (index+1);
                if(next < length){
                    $("#tabs").tabs('select',next);
                }else{
                    $("#tabs").tabs('select',0);
                }
                $(".toScroll")[0].scrollTo($('li.ui-state-active').position().top);
            }

            function clickPrevious(index){
                var length  =   $("#tabs").tabs('length');

                if(index == 0){
                    $("#tabs").tabs('select',(length-1));
                }else{
                    $("#tabs").tabs('select',(index-1));
                }
                $(".toScroll")[0].scrollTo($('li.ui-state-active').position().top);
            }
            
            function open_win(url_add)
			{
			   window.open(url_add,'Editmap','width=1200,height=600,menubar=no,status=yes,location=no,toolbar=no,scrollbars=yes');
			}
            
        /*  function saveGlobalVariable(formval){
       
          	var url_data = 'simpleui.m?'+$(formval).serialize();
          	$.post(url_data,function(data){
          		//alert(data);
          		$(".statusmsg").empty().show().text(data);
                 setTimeout('$(".statusmsg").hide("slow").empty()','2000');
          	});
          }  
          */
           function saveGlobalVariable(formval){
           		var url_data = 'simpleui.m?'+$(formval).serialize();
           		 var mytime= "&ms="+new Date().getTime();
		          $.ajax({
				  url: url_data+mytime,
				  type: "POST",
				  success: function(data){
				  	$(".statusmsg").empty().show().text(data);
               	    setTimeout('$(".statusmsg").hide("slow").empty()','2000');
				//    alert('success');
				  },
				  error: function(xhr,status,error){
				  	
				    //alert('Session Expired. Please login again.');
				    if(xhr.status==200){
        		  		alert('Session expired. Please login once again');
	        		}else{
	                	alert('Saving failed');
		        	}
				  }
				});
           }
          
          function reloadPage(location){
			var radios = document.getElementById('questionares');
			if (radios) {
			  var inputs = radios.getElementsByTagName ('input');
			  if (inputs) {
			    for (var i = 0; i < inputs.length; ++i) {
			      if (inputs[i].type == 'radio' && inputs[i].name == 'readergroup'){
			       		//inputs[i].checked = inputs[i].value;
			       		location+= '&readergroup='+inputs[i].value;
			       		//alert('location::'+location);
			        }
			    }
			  }
			}
		 	window.location.href = location;
		}
		
	function setRadio(elmtRef,formRef) {    
	  if (elmtRef.name == "Expire_Date" && formRef.ExpirationDate.value == "Permanent") {
	    formRef.ExpirationDate.focus();
	  } else {
	    if (elmtRef.value == "Permanent") {
	      formRef.Expire_Date.value = "";
	     // formRef.Expire_Date.style.background = "rgb(200,200,200)";
	    } else {
	      formRef.Expire_Date.style.background = "white";
	      formRef.Expire_Date.focus();
	    }
	  }
	}
		