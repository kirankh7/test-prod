/**
 * @fileoverview This file handles all the operations regarding the json data
 * 
 *
 * @author rohith rohith@iframetechnologies.com
 * @version 0.1
 */


uname=profileName;
/**
 * This is the global variable for checking whether the state diagram is saved or not.
 * when a change occurs in the diagram the save_state is set to false
 */
save_state=true;
/**
 * This is the global variable for checking whether the action diagram is saved or not.
 * when a change occurs in the diagram the save_action is set to false
 */
save_action=true;
/**
 * profile_json is global object for manipulating peofile json
 */
profile_json=undefined;
/**
 * Gives the current state that is being processed
 */
update_state=undefined;
/**
 * Gives the current action that is being processed
 */
update_action=undefined;






function load_tree(){
    var d= new Date();
    var mytime= "&ms="+new Date().getTime();
   //   alert(tree_url);
 
    $.get(tree_url+mytime,function(data){
       // alert(data.toSource());
       

        $('.root').html('');
        $('.root').createAppend(

            "span",{
                "id" :"tree_user"
            }, uname
        
            );
        //alert($('.root').createAppend(eval(data)).toSource()); 
        //  $('.root').createAppend(     
    //    alert(data.toSource());
        $('.root').createAppend('ul', { } ,eval(data));
       /*        $('.root').createAppend("ul", { } ,
   [
      "li", {"id" : "json/process-defenition.json" } , "pdl1",
      "li", {"id" : "json/process-defenition2.json" } , "pdl2",
      "li", {"id" : "json/pdl3.json" } , "pdl3",
      "li", {"id" : "json/aj.json" } , "aj",
      "li", {"id" : "json/kklkll.json" } , "new"
   ]);*/
   if($('.root>ul>li>span').length < 1){
   		
   		$(".icon_menu_view").hide();
   		$(".icon_menu_view2").show();
   }
   
        $('#tree_user').html(uname);

        	simpleTreeCollection = $('.simpleTree').simpleTree({
            autoclose: false,
            animate:true,
            drag: false
            
        });        
        $('#tree_user').contextMenu({
            menu: 'root_menu'
        },
        function(action, el, pos){           
            switch(action){
                case 'add_pdl'  :
                    add_pdl();
                    break;   
                case 'upload_pdl':
                    upload_pdl_page();
                    break;
            }      
        }
        );
        $('.root>ul>li>span').contextMenu({
            menu: 'navigation_menu'            
        },
        
        function(action, el, pos){
            //alert($(el).attr('id'));
            switch(action){
                case 'add_pdl'  :
                    add_pdl();
                    break;
                case 'delete_pdl'   :
                    //    delete_pdl($(el).parent().attr('id'));
                    delete_pdl($(el).parent().attr('delpdl'));
                    break;
                case 'open_pdl'  :	
                    open_pdl($(el).parent().attr('id'));
                    break;
                /*         case 'rename_pdl'  :
                    rename_pdl($(el).parent().attr('id'));
                    break;
                    **/
                case 'download_pdl':
                    download_pdl($(el).parent().attr('downloadpdl'));
                    break;
                case 'editsession_pdl':
                    edit_session_pdl($(el).parent().attr('edit_session'));
                    break;
                case 'validate_pdl':
                    //alert("validate pdl clicked");
                    validate_pdl((el).parent().attr('validatepdl'));
                    break;
                case 'upload_pdl':
                    //alert("Upload pdl clicked");
                    //upload_pdl_page($(el).attr('uploadpdl'));
                    upload_pdl_page();
                    break;
            }
        //$(el).stopPropagation();
        }
        );
        
        // $('.simpleTree li.root').disableContextMenu();
        
        //    $('#tree_user').click(function(){
        //        add_pdl();
        //   });
        $('.root>ul>li>span').dblclick(function(){        	
        	//alert('dbsle');
            //open_pdl($(this).attr('id'));
            open_pdl($(this).parent().attr('id'));
        });
        
    });
    
}
function validateActive(checkActivePdlIcon,action){	
	if($(checkActivePdlIcon).parent().attr('id')==undefined && action!='add_pdl'){
	alert("Please select the process definition first by clicking the navigation tree below.");
	return false;
	}else{
		//('open_pdl', checkActivePdlIcon, '');			
		switch(action){
                case 'add_pdl'  :
                    add_pdl();
                    break;
                case 'delete_pdl'   :
                    //    delete_pdl($(el).parent().attr('id'));
                    delete_pdl($(checkActivePdlIcon).parent().attr('delpdl'));
                    break;
                    
                case 'open_pdl'  :	
                    open_pdl($(checkActivePdlIcon).parent().attr('id'));
                    break;
                /*         case 'rename_pdl'  :
                    rename_pdl($(el).parent().attr('id'));
                    break;
                    **/
                case 'download_pdl':
                    download_pdl($(checkActivePdlIcon).parent().attr('downloadpdl'));
                    break;
                case 'editsession_pdl':
                    edit_session_pdl($(checkActivePdlIcon).parent().attr('edit_session'));
                    break;
                case 'validate_pdl':
                    //alert("validate pdl clicked");
                    validate_pdl($(checkActivePdlIcon).parent().attr('validatepdl'));
                    break;
                case 'upload_pdl':                    
                    upload_pdl_page();
                    break;
		}
	//return true;
	}
}
function word_wrap(word){
    if(word.length>=9){
        return word.substr(0,8)+"...";
    }
    else{
        return word;
    }
}

function clk_adaptor_edit(param,currentAdapterType){
	var mytime= "&ms="+new Date().getTime();
   /*     $.get('dbsyncprofiledefinition.m?command=propertysection&profileName='+profileName+'&selectedAdapter='+param+mytime,function(data){
            $('#bp_property_1').html(data);
            $('#bp_property_1').slideDown('slow');
            $('#bp_property_2').hide();
            //    $("#save_adapter_property").show();
            save_property(param,currentAdapterType);
        });
	*/

    $.ajax({
        url:'dbsyncprofiledefinition.m?command=propertysection&profileName='+profileName+'&selectedAdapter='+param+mytime,
        success:function(data){
    	//alert(data);
            $('#bp_property_1').html(data);
            $('#bp_property_2').hide();
            $('.active_processes').hide();
            $('.console_running_processes').hide();
            $('#bp_property_1').slideDown('slow');
            $(window).scrollTo(600,500);
            save_property(param,currentAdapterType);
        }
    });
    
   
}

function clk_adaptor_delete(param){
	var mytime= "&ms="+new Date().getTime();
    $("#confirm_dialog").html("Do you want to delete adapter?");
    $("#confirm_dialog").dialog('option','title',"Delete Adapter");    
    $("#confirm_dialog").dialog('option','buttons',{
        'Ok':function(){
            $(this).dialog('close');
            $.ajax({
                type: "GET",
                url: 'dbsyncprofiledefinition.m',
                data:'profileName='+profileName+'&selectedAdapter='+param+'&command=deleteAdapter'+mytime,
                //  dataType:'json',
                cache:false,
                success:function(data){
                    //    message_box('Delete Adaptor','Status: '+data.status+"<br>Message: "+data.message);
                    //	alert(data+"---");
                    list_adaptors();
                    message_box('Delete Adapter','Adapter deleted successfully');
                    
                },
                error:function(xhr,status,error){
                    //   message_box('Delete Adaptor',status+"<br>"+xhr.status+' : '+xhr.statusText);
                    message_box('Delete Adapter','Adapter deletion failed');
                    list_adaptors();
                }
            });
        },
        'Cancel':function(){
            $(this).dialog('close');
        }
    });
    $("#confirm_dialog").dialog('open');
    
}

function list_adaptors(){
	
	var mytime= "&ms="+new Date().getTime();
    // alert("simple test");
    $.ajax({
        type: "GET",
        url: adaptor_url,
        data:'action=list'+mytime,
        dataType:'json',
        cache:false,
        success:function(data){
        	//alert(data.toSource());
            uname=data.profile.name;
            $("#adapter_table").html('');
       		
            var rowsTmp ='';
           
            for(i=0; i<data.profile.adapter.length;i++){
            	
                currentAdapterType = data.profile.adapter[i].type;
                var hasValidate = data.profile.adapter[i].hasValidate;
              
               var adapterType=new Array();
                 adapterType = currentAdapterType.split('.');
               if(adapterType.length > 0){
                 adapterType = adapterType[adapterType.length -1];
               }else{
               	adapterType = '';
               }
                if(adapterType=='QuickBooksAdapter2'){
                    adapterType='QuickBooksAdapter';
                }             
                rowsTmp += "<tr>"+
                "<td class='cd_span_1'>"+data.profile.adapter[i].name+"</td>" +
                "<td class='cd_span_2'><img src='images/qb.png' style='padding-right:4px'/>"+adapterType+"</td>" +
                "<td class='cd_span_3'><a onclick='clk_adaptor_edit(\""+data.profile.adapter[i].name+"\",\""+currentAdapterType+"\")'  href='#'> " +
                "<img src='images/pencil.png'/><span class='txt_after_img'>Edit</span></a></td>";
                if(isEditable){
                    rowsTmp +=	"<td class='cd_span_4'><a onclick='clk_adaptor_delete(\""+data.profile.adapter[i].name+"\")'  href='#'> " +
                    "<img src='images/delete.png'/><span class='txt_after_img'>Delete</span></a></td>" ;
                }else{
                    rowsTmp +=	"<td class='cd_span_4'>" +
                "<img src='images/delete.png'/><span class='txt_after_img' style='color:gray;'>Delete</span></td>" ;
                }
  				
                if(hasValidate==true){
                	
                    rowsTmp +=	"<td class='cd_span_5'><a href='#' onclick='validateConnectionProperties(\""+data.profile.adapter[i].name+"\",\""+adapterType+"\")'><img src='images/arrow_refresh_small.png'/><span class='txt_after_img'>Validate</span></a></td>";
                /*"<td class='cd_span_6'> </td>" ;*/
                }
                else{
                    rowsTmp += "<td>&nbsp;</td>";
                }
                rowsTmp += "</tr>";
                		            
            }
            //alert(rowsTmp);
            //$("#mydiv table").append(rowsTmp);
            $("#adapter_table").append(rowsTmp);
        },
        error:function(xhr,status,error){
        	if(xhr.status==200){
        		  message_box('List Adaptor','Session expired. Please login once again');
        	}else{
            message_box('List Adaptor',status+"<br>"+xhr.status+' : '+xhr.statusText);
        	}
        }
    });
}

function save_property(selectdAdapter,currentAdapterType){	
	var mytime= "&ms="+new Date().getTime();
    $("#property_adaptor_form :button").click(function(){    	    
        $.ajax({
            type: "GET",
            url: $("#property_adaptor_form").attr('action'),
            data:"command=saveAdapterProperties&profileName="+profileName+"&"+$("#property_adaptor_form").serialize()+mytime,         	
            cache:false,
            success:function(data){
              /*  if(data.status=="success"){
                    message_box('Property',data.message);                 	
                    $('#bp_property_1').html("").hide('slow');
                    $("#save_adapter_property").hide();
                }*/
               // message_box("Adapter property"," Saved Successfully");
               message_box3('Saved Successfully.','.show_message_sucess');
                $('#bp_property_1').html("").hide('slow');
                $("#save_adapter_property").hide();
            },
            error:function(xhr,status,error){
            	if(xhr.status==200){
        		  message_box('List Adaptor','Session expired. Please login once again');
	        	}else{
	                message_box('Load PDL', status+"<br>"+xhr.status+' : '+xhr.statusText);
	        	}
            }
        });
    });
}

function open_pdl(el_id){	
	
    //alert(el_id);
    window.open_url=el_id;
    selected_pdl_name = el_id.split("pdlJsonFile=")[1];
   // var mytime= "&ms="+new Date().getTime();
    // pdl_name_global = selected_pdl_name;
    //document.myform.pdlName.value = profileValue;
    if(save_state ){    
        file_name=el_id;
        reset_canvas();
        $('#upload_pd').hide();
        $('.run_pdl_button').show();

        $("#tab_datasource").hide();
        $("#button_save_state").show();
        $("#button_close_state").show();
        $("#button_back_action").hide();
        $("#button_save_action").hide();
        $("#tab_controlflow").addClass('active').show();
        $(".run_pdl_button").show();
        //reset scroll position
        $("#cc_2").scrollTop(0);
        $("#cc_2").scrollLeft(0);
        
		
        var d=new Date();
        //test = {"process_definition":{"name":"%1$s","state":[{},{}],"end_state":{"id":"endid","name":"end_state","connections":{"cout":[{},{}],"cin":[{"id":"startid"},{}]},"coordinates":{"left":"200","z":"99","top":"200"}},"start_state":{"id":"startid","name":"start_state","connections":{"cout":[{"id":"endid"},{}],"cin":[{},{}]},"coordinates":{"left":"50","z":"99","top":"50"}}}};
		
		
        $.ajax({
            type: "GET",
            url: el_id,
            data :"ms="+new Date().getTime(), 
            dataType:'json',
            cache:false,           
            success: function(data){        
               // alert(data.toSource());
                profile_json=data;
                load_control_diagram(data);
                tab_controlflow_click();
            },
            error:function(xhr,status,error){
            	if(xhr.status==200){
        		  message_box('Load PDL','Session expired. Please login once again');
	        	}else{
                message_box('Load PDL', status+"<br>"+xhr.status+' : '+xhr.statusText);
	        	}
            }
        });
        
    }
    else{
        message_box('Save','Not Saved<br>First save the current diagram');
    }
}
function pdl_json_update(el_id,action_id_up){	
	    
    window.open_url=el_id;
    var mytime= "&ms="+new Date().getTime();
    // selected_pdl_name = el_id.split("pdlJsonFile=")[1];
    //document.myform.pdlName.value = profileValue;
    if(save_state ){    
        file_name=el_id;
        reset_canvas();
        //$("#tab_connection").hide();
       
        
		
        var d=new Date();
		
		
		
        $.ajax({
            type: "GET",
            url: el_id+mytime,
            dataType:'json',
            cache:false,           
            success: function(data){        
                //alert(data.toSource());
                profile_json=data;
                load_action_diagram(action_id_up);
                updateCanvas2($("#canvas"), $(".blk"));
                message_box3('Saved Successfully.','.show_message_sucess');
            //tab_controlflow_click();
            },
            error:function(xhr,status,error){
            	if(xhr.status==200){
        		  message_box('Load Updated PDL','Session expired. Please login once again');
	        	}else{
                message_box('Load Updated PDL', status+"<br>"+xhr.status+' : '+xhr.statusText);
	        	}
            }
        });
        
    }
    else{
        message_box('Save','Not Saved<br>First save the current diagram');
    }
}
function setProcessName(data){
	$('.process_name').text(data);
}
function stop_sync(processName){
	 var kill_url='consolelogviewer.m?command=kill&processName='+processName;
	 var mytime= "&ms="+new Date().getTime();
	 $.ajax({
         type: "GET",
         url: kill_url+mytime,               
         cache:false,
         success:function(data){
         	//data = $.trim(data);
         	//setProcessName(data);
         	//setIframeSrcValue('consolelogviewer.m?command=kill&processName='+data);
        	 $('.stop_pdl_button').hide();
        	 $('.run_pdl_button').show();
        	 $('.active_process').click();
        	 
         },
         error:function(xhr,status,error){                  
             message_box('Stop Sync Status','Failed to stop.');                    
         }
     });
}
function run_pdl(){
	
    var run_url='runpdl.m?profileName='+uname+'&processFileName='+selected_pdl_name+'&command=start';
     var mytime= "&ms="+new Date().getTime();
  //  window.open(run_url);
    $.ajax({
                type: "GET",
                url: run_url+mytime,               
                cache:false,
                success:function(data){
                	data = $.trim(data);
                	//alert(data+'---run pdl output');
                	$('.run_pdl_button').hide();
                	$('.stop_pdl_button').show();
                	setProcessName(data);
                  // message_box('Run Status','Your request has been started.You will receive an email once it is completed.');
                     //message_box('Run Status','Your request has been submitted. Please wait till the process gets completed.');
                	setIframeSrcValue('consolelogviewer.m?command=viewlog&processName='+data);
                },
                error:function(xhr,status,error){                  
                    message_box('Run Status','Failed to run.');                    
                }
            });
}
function add_pdl(){
    $("#prompt_dialog input").val('');
    prompt_box('Add Process Definition',"Enter Name",add_ok);
    
}

function delete_pdl(el_id){	
	  var mytime= "&ms="+new Date().getTime();
    $("#confirm_dialog").html("Do you want to delete PDL?");
    $("#confirm_dialog").dialog('option','title',"Delete PDL");
    $("#confirm_dialog").dialog('option','buttons',{
        'Ok':function(){
            $(this).dialog('close');
            $.ajax({
                type: "GET",
                url: el_id+mytime,
                //     data:"action=delete&name="+el_id,
                //    dataType:'json',
                cache:false,
                success:function(data){
                    load_tree();
                    message_box('Delete','Process Definition deleted successfully.');
                   
                },
                error:function(xhr,status,error){
                    //  message_box('Delete',status+"<br>"+xhr.status+' : '+xhr.statusText);
                    message_box('Delete','Process Definition deletion failed.');
                    load_tree();
                }
            });
        },
        'Cancel':function(){
            $(this).dialog('close');
        }
    });
    $("#confirm_dialog").dialog('open');
}

function download_pdl(el_id){
    //alert(el_id);
    window.open(el_id);
//     $.post(el_id);
}
function edit_session_pdl(el_id){
    window.open(el_id,'mywin','left=20,top=20,width=500,height=300,toolbar=1,resizable=0');
}
function rename_pdl(state_id){
    $("#prompt_dialog input").val('');
    prompt_box('Rename',"Enter new name for the state.",rename_ok);
    window.state_id=state_id;
    
}
function rename_dataflow_block(type,dataflow_blockid){
	//alert(dataflow_blockid);
    $("#prompt_dialog input").val('');
    prompt_box('Rename',"Enter new name for the "+type+".",rename_df_ok);
    window.dataflow_blockid=dataflow_blockid;
    
}
function validate_pdl(validate_url){
    //alert(validate_url);
    
    window.open(validate_url,'validatestatuswindow','left=20,top=20,width=1000,height=1000,toolbar=1,resizable=yes,scrollbars=yes ');
    
}
function upload_pdl_page(){
    //	alert('dbsyncprofiledefinition.m?command=popupcreateprofile&profileName='+uname);
    //	 $("#prompt_dialog input").val('');
    //    prompt_box('Upload Process File',"Select Process File",rename_ok);

    $("#upload_pd").colorbox(
    {
        href:'dbsyncprofiledefinition.m?command=popupcreateprofile&profileName='+uname
    //onComplete: true
    }, prepform
	
    );
    $.fn.colorbox.close();
/*	$("#upload_pd").colorbox({
        href:'dbsyncprofiledefinition.m?command=popupcreateprofile&profileName='+uname,
        close: "Close",
       // iframe :true,
        overlayClose: false,
        scalePhotos: true,
        height:'180px',
        innerWidth: 'auto',
        opacity: 0.6,
       
    }); */
// $.fn.colorbox.close();

}
function prepform(){	
    $("form").ajaxForm({
        success:
        function(html){
            $.fn.colorbox({
                html:html,
                open:true
            });
        // $.fn.colorbox.close();
        }
    });
   
} 
function message_box(title,message){    
    $("#message_dialog").html(message).dialog('option','title',title).dialog('open');
}

function prompt_box(title,label,fn){
    $("#prompt_dialog label").html(label);
    $("#prompt_dialog").dialog('option','title',title);
    $("#prompt_dialog").dialog('option','buttons',{
        'Ok':fn,
        'Cancel':function(){
            $(this).dialog('close');
        }
    });
    $("#prompt_dialog").dialog('open');
}

function rename_ok(){	
    new_name=$("#prompt_dialog input").val();
   //  var mytime= "&ms="+new Date().getTime();
    if(new_name==''){
        message_box('Error','Cannot be Empty');
    }
    else{
        $("#prompt_dialog").dialog('close');
        $.ajax({
            type: "GET",
            url: pdl_url,
            data:"command=renamepdl&oldName="+window.state_id+"&newName="+new_name+"&profileName="+uname+"&pdl="+selected_pdl_name+"&ms="+new Date().getTime(),
            // dataType:'json',
            cache:false,
            success:function(data){            	
                data = $.trim(data);
                if(data == 'Success'){                	
                   // message_box('Rename','Successfully updated.');     
                    message_box2('Successfully updated.');              
                    open_pdl(open_url);
                }
                if(data == 'Failure'){
                   // message_box('Rename','Update Failed.');
                   message_box2('Update Failed.');
                }
            },
            error:function(xhr,status,error){
            	if(xhr.status==200){
        		  message_box('Rename','Session expired. Please login once again');
	        	}else{
                message_box('Rename',status+"<br>"+xhr.status+' : '+xhr.statusText);
	        	}
            }
        });
    }
}
function rename_df_ok(){	
	new_name=$("#prompt_dialog input").val();
    if(new_name==''){
        message_box('Error','Cannot be Empty');
    }
    else{
        $("#prompt_dialog").dialog('close');
        $.ajax({
            type: "GET",
            url: pdl_url,
            data:"command=renamedataflow&oldName="+window.dataflow_blockid+"&newName="+new_name+"&profileName="+uname+"&pdl="+selected_pdl_name+"&ms="+new Date().getTime(),
            // dataType:'json',
            cache:false,
            success:function(data){            	
                data = $.trim(data);
                if(data == 'Success'){                	
                   // message_box('Rename','Successfully updated.');
                   message_box2('Successfully updated.');                   
                   // open_pdl(open_url);
                   pdl_json_update(open_url,action_id);
                }
                if(data == 'Failure'){
                   // message_box('Rename','Update Failed.');
                     message_box2('Update Failed.'); 
                }
            },
            error:function(xhr,status,error){
            	if(xhr.status==200){
        		  message_box('Rename','Session expired. Please login once again');
	        	}else{
                message_box('Rename',status+"<br>"+xhr.status+' : '+xhr.statusText);
	        	}
            }
        });
    }
}
function add_ok(){
    new_name=$("#prompt_dialog input").val();
  //   var d= new Date();
  
  var mytime= "&ms="+new Date().getTime();
    /*   if(new_name==''){
        message_box('Error','Cannot be Empty');
    }*/
    // if empty 
    if(/^\s*$/.test(new_name) ) { 
        message_box('Error','Cannot be empty.');
    }else if(/[\s]/.test(new_name)){ //if contains a space    
        message_box('Error','Cannot contain space.');
    }
    else{
        $("#prompt_dialog").dialog('close');	
        /*var pdlFileNames = new Array();
			pdlFileNames = pdlFileNamesList.replace("[","").replace("]","").split(",");
			var isDuplicate =new Boolean(false);			
				isDuplicate = checkDuplicatePDLName(pdlFileNames,new_name);*/
        isDuplicate = pdl_duplicate(new_name);
				
				
				
				
        if(!isDuplicate){
            $.ajax({
                type: "GET",
                url: pdl_url,
                data:"command=createPDL&pdl="+new_name+"&profileName="+uname+mytime,
                //   dataType:'json',
                cache:false,
                success:function(data){
                    message_box('Add Process Definition','Successfully Added');
                    load_tree();
                    if(data.status=="success"){
                        message_box('Add','Status: '+data.status+"<br>Message: "+data.message);
                        load_tree();
                        file_name='json/'+new_name+'.json';
                    //  alert(file_name);
                    //load_control_diagram();
                    }
                    if(data.status=='failure'){
                        message_box('Add','Status: '+data.status+"<br>Message: "+data.message);
                    }
                },
                error:function(xhr,status,error){
                    //  message_box('Add',status+"<br>"+xhr.status+' : '+xhr.statusText);
                    load_tree();
                }
            });
        }else{
            message_box('Add','Already same process definition name is present.Please type some other name to create a new process definition');
        }
    }

}

function load_control_diagram(data){
    //alert($.toJSON(data));
	try{
    //data = $.toJSON(data);
    //alert(data.process_definition.name.toSource()+"**");
    //alert("Cool"+$.toJSON(data));
    window.profile_name=data.process_definition.name;      
    var start=data.process_definition.start_state;    
    var end=data.process_definition.end_state;
   // alert(start.toSource()+"++"+end.toSource()+"+-------------+"+window.profile_name);
    draw_control(start, 'true');	
 
    $(data.process_definition.state).each(function(index,st){
       // alert(st.isActive+"-------inside load_control_diagram"+st);
        draw_control(st,st.isActive);
    });
  
   // alert("end State"+end);
    draw_control(end, 'true');
    draw_control_connection(data);

    }catch(err){
       // alert('load_control_diagram:::'+err);
    }

}

function draw_control(el,active){
    //alert(el.toSource()+"-------------draw_control"+el.name);
    try{
    var top  = parseInt(el.coordinates.top)+$("#cc_2").scrollTop()
    var left = parseInt(el.coordinates.left)+ $("#cc_2").scrollLeft()
	
    var coordinates = {
        'top' : top,
       
        'left': left
    };
  //	alert(el.id+'--------'+el.name+'----------------'+active);  	
    add_new_box_control( el.id , el.name, coordinates,'state',active );
  	 }catch(err){
       // alert(err);
    }
}

/*function draw_control_connection(data){
    var start=data.process_definition.start_state.id;
  
    var end=data.process_definition.start_state.connections.cout[0].id;  
    var cin=undefined;
    connect_draw(start,end,cin);
    $(data.process_definition.state).each(function(index,st){    	 
          	    	
        start=st.id;
        end=st.connections.cout[0].id;
        cin=st.connections.cin[0].id; 
            
        connect_draw(start,end,cin);
    	
    });    
    start=data.process_definition.end_state.id;   
    end=undefined;
    cin=data.process_definition.end_state.connections.cin[0].id;  
    connect_draw(start,end,cin);
}*/

function draw_control_connection(data){
    var start=data.process_definition.start_state.id;
    var end=data.process_definition.start_state.connections.cout[0].id;
    //var end_name = data.process_definition.state.id
   //  alert(data.toSource()+"-------draw_control_connection");
    var end_name='';
    for(i=0;i<data.process_definition.state.length;i++){
    	//alert(data.process_definition.state[i].id+"-------Inside draw_control_connection--"+end);
        if(data.process_definition.state[i].id== end){
            end_name=data.process_definition.state[0].name;
        }else if(data.process_definition.end_state.id==end){
            end_name=data.process_definition.end_state.name;
        }
    }
    
    var cin=undefined;
    connect_draw(start,end,cin,end_name);
    $(data.process_definition.state).each(function(index,st){
        //alert(st.toSource()+"draw_control_connection inside state");
        start=st.id;
        end=st.connections.cout[0].id;
        cin=st.connections.cin[0].id;
        var end_name='';
        for(var j=0;j<data.process_definition.state.length;j++){    	
            if(data.process_definition.state[j].id== end){
                //alert();
                end_name=data.process_definition.state[j].name;
            }else if(data.process_definition.end_state.id==end){
                end_name=data.process_definition.end_state.name;
            }
        }
        connect_draw(start,end,cin,end_name);
        
    });
    start=data.process_definition.end_state.id;
    end=undefined;
    cin=data.process_definition.end_state.connections.cin[0].id;
    connect_draw(start,end,cin,'');
    
}
function save_now(){
    //pdl_name_global = selected_pdl_name;
    var mytime= "&ms="+new Date().getTime();
    $('.run_pdl_button').show();
    check_orphan_state();
    //alert(profile_json);
    if(!orphan_state){    	
        //alert("Save Now"+$.toJSON(profile_json));
        $.ajax({
            type: "POST",
            url: save_url,
            data:"pdlJsonObject="+escape($.toJSON(profile_json))+"&profileName="+uname+"&fileName="+file_name+"&saveStateVisible="+$(".right_panel_first_child #button_save_state").is(':visible')+mytime,
            // dataType:'json',
            cache:false,
            success: function(data){          
                //message_box('Save','Successfully saved.');
                message_box2('Successfully saved.');
                load_tree();
                save_state=true;
                if(data.status=="success"){
                    //  message_box('Save','Status: '+data.status+"<br>Message: "+data.message);                    
                   // message_box('Save','Successfully saved.');
                    message_box2('Successfully saved.');
                    load_tree();
                    save_state=true;
                }
                if(data.status=='failure'){
                    message_box('Save','Status: '+data.status+"<br>Message: "+data.message);
                }
            },
            error:function(xhr,status,error){
            	if(xhr.status==200){
        		  message_box('Save PDL','Session expired. Please login once again');
	        	}else{
                message_box('Save PDL', status+"<br>"+xhr.status+' : '+xhr.statusText);
	        	} 
            }
        });
    }
    else{
        message_box('Error',"Detected orphan states or action<br>First connect or remove orphans");
    }

}

/**
 * Constructs a process definition object
 * @class This is base class for a profile
 * This class requires a instance of {@link profile_obj}
 * @constructor
 */
function pdl_obj(){
    this.process_definition=new profile_obj(window.profile_name);
}

/**
 * Constructs a profile json object
 * @extends pdl_obj
 * This is base class for json object
 * This class requires name as parameters and have three states
 * start state
 * end state
 * array of states. This is where all states will stored
 * @constructor
 * @param {string} name of the profile
 */
function profile_obj(name){
    this.name=name;
    this.start_state;
    this.end_state;
    this.state=new Array();
}

function start_state_obj(name, id, coordinates, connect){
    this.name=name;
    this.id=id;
    this.coordinates=coordinates;
    this.connections=connect;
}

function end_state_obj(name, id, coordinates, connect){
    this.name=name;
    this.id=id;
    this.coordinates=coordinates;
    this.connections=connect;
}

function state_obj(name, id, event, isActive, coordinates, connect){
    this.name=name;
    this.id=id;
    this.isActive=isActive;
    this.event=event;
    this.coordinates=coordinates;
    this.connections=connect;
}

function coord_obj(left, top, z){
    this.left=left;
    this.top=top;
    this.z=z;
}

function connect_obj(){
    this.cin=new Array();
    this.cout=new Array();
}

function cin_obj(id){
    this.id=id;
}

function cout_obj(id){
    this.id=id;
}

function event_obj(type){
    this.type=type;
    this.action=new Array();
}

function action_obj(type,name,id,coordinates,connections,displayname){
    this.type=type;
    this.name=name;
    this.id=id;
    this.displayname = displayname;
    this.properties=new Array();
    this.coordinates=coordinates;
    this.connections=connections;
    
}

function property_obj(){
    this.property_name;
    this.property_value;
}

function load_action_diagram(eid){
   // alert("load_action_diagram---------------"+eid);
   try{
    action_id=eid;
    

    if(profile_json.process_definition.start_state.id==action_id || profile_json.process_definition.end_state.id==action_id){
        message_box("Notice","Start or End state selected which doesnot have actions");
    }
    else{
        if(save_state){
            reset_canvas();
            //$("#tab_controlflow").hide();
            $('#button_save_state').hide();
            $('#button_close_state').hide();
            $('#button_save_action').show();
            $('#button_back_action').show();
            $("#tab_datasource").addClass('active').show();
            $('.stop_pdl_button').hide();
            $('.run_pdl_button').show();
            $(profile_json.process_definition.state).each(function(index,st){
                //alert(eid+"Inside load_action_diagram-----------"+st.id);
                if(st.id==eid){
                    update_state=st;
                    if(update_state.event.action == undefined){                  
                        update_state.event.action = new Array();
                    }
                    //alert(update_state.toSource()+"---Action");
                    if($(update_state.event.action).length > 0){
                        $(update_state.event.action).each(function(i,action){
                            draw_action(action);
                        });
                        
                    }
                    draw_action_connection(update_state);
                }        
            });
            tab_datasource_click();

        }
        else{
            message_box('Save','First save your states');
        }
        
        $('.run_pdl_button').show();
    }
   }catch(err){
   		alert('error loading action diagram'+err);
   }

}

function draw_action(el){
   // alert(el.toSource()+"----Inside draw_action----");
    try{
        coordinates = {
            'top' : parseInt(el.coordinates.top)+$("#cc_3").scrollTop(),
            'left': parseInt(el.coordinates.left)+$("#cc_3").scrollLeft()
        };
        if(el.displayname==undefined){
        		el.displayname = '';
        }
        if(el.type=='map'){        	
            add_new_box_map( el.id , el.name, coordinates, el.type,el.displayname);
        }else{
            add_new_box( el.id , el.name, coordinates, el.type,el.displayname);
        }
    }catch(err){
        //alert(err);
    }
    
}

function draw_action_connection(data){
    //alert("inside draw_action_connection"+data.toSource());
    try{
        $(data.event.action).each(function(index,act){
           // alert("inside draw_action_connection act----"+act.toSource());

            $(act.connections.cin).each(function(i,el){
                $($(gebid(act.id)).find('p ul.incoming')).append('<li rel = '+el.id+'> Connect from '+el.id+'</li>');
            });
            $(act.connections.cout).each(function(i,el){
                if(act.type=='reader'){
                    var readerCoutId = word_wrap(el.id);
                    //$($(gebid(act.id)).find('p ul.outgoing')).append('<li rel = '+el.id+'> Connect to '+el.id+'<a class="key" onclick="remove_connection_action(this)">[x]</a></li>');
                    $($(gebid(act.id)).find('p ul.outgoing')).append('<li rel = '+el.id+'>'+readerCoutId+'<a class="key" onclick="remove_connection_action(this)">[x]</a></li>');
                }else{
                    if(act.type=='map'){
                        var sequenceno = act.sequenceno;
                        if(sequenceno==undefined){
                            sequenceno = '-';
                        }
                        $($(gebid(act.id)).find('p ul.outgoing')).append('Sequence No '+sequenceno+'<li rel = '+el.id+'> Connect to <a class="key" onclick="remove_connection_action(this)">[x]</a></li>');
                    }else{
                        $($(gebid(act.id)).find('p ul.outgoing')).append('<li rel = '+el.id+'> Connect to <a class="key" onclick="remove_connection_action(this)">[x]</a></li>');
                    }
                }
            });
        });
    }catch(err1){
      //  alert(err1);
    }
	
    updateCanvas2($("#canvas"), $(".blk"));
}

function state_update_drag(id,pos){
	 try{
    //alert("inside state_update_drag");
    find_state(id);
    pos.top = pos.top+$("#cc_2").scrollTop();
    pos.left = pos.left+$("#cc_2").scrollLeft();	    
    update_state.coordinates=pos;
    }catch(err1){
       // alert(err1);
    }
}

function state_update_active(id,value){	
    id = $.trim(id);
    //alert(id+"id"+"-----------"+"value"+value);
    find_state(id);
    update_state.isActive=value;
}

function state_update_remove(start_id, end_id){
	try{
    find_state(start_id);
    var start_state=update_state;
    find_state(end_id);
    var end_state=update_state;

    start_state.connections.cout.pop();
    end_state.connections.cin.pop();
     }catch(err1){
        alert(err1);
    }
}

function state_update_connect(start_id, end_id){
	try{
    find_state(start_id);
    var start_state=update_state;
    find_state(end_id);
    var end_state=update_state;

    start_state.connections.cout.push(new cout_obj(end_id));
    end_state.connections.cin.push(new cin_obj(start_id));
	 }catch(err1){
       // alert(err1);
    }
//print_json();
}

function state_update_remove_box(eid){	
    //alert(eid+"---"+$.toJSON(profile_json.process_definition.state));
    try{
    $(profile_json.process_definition.state).each(function(i,state){
        if(state.id == eid){
            //alert(array_index+"-----array_index----");
            array_index= i;
        }
    });
    profile_json.process_definition.state.splice(array_index,1);
     }catch(err1){
        //alert(err1);
    }
//print_json();

}

function find_state(id){
	 try{
    if(profile_json.process_definition.start_state.id == id){
    	
        update_state= profile_json.process_definition.start_state;
    // alert("inside find_state1"+update_state);
    }
    else if(profile_json.process_definition.end_state.id == id){
        update_state= profile_json.process_definition.end_state;
    //alert("inside find_state2"+update_state);
    }
    else{
        return $(profile_json.process_definition.state).each(function(i,state){
            if(state.id == id){
                update_state= state;
            }
        // alert("inside find_state3"+update_state);
        });
    }
   }catch(err1){
        //alert(err1);
    }
}

function action_update_drag(id,pos){
    //alert("Im dragged");
   try{
    find_action(id);

    pos.top = pos.top+$("#cc_3").scrollTop();
    pos.left = pos.left+$("#cc_3").scrollLeft();


    update_action.coordinates=pos;
    }catch(err1){
        //alert(err1);
    }
}

function action_update_remove(start_id, end_id){
	try{
    find_action(start_id);
    var start_action=update_action;
    find_action(end_id);
    var end_action=update_action;

    start_action.connections.cout.pop();
    // end_action.connections.cout.pop();
    end_action.connections.cin.pop();
    }catch(err1){
       // alert(err1);
    }
}

function action_update_connect(start_id, end_id){
    //alert(start_id+"Inside action_update_connect"+end_id);
    try{
    find_action(start_id);
    var start_action=update_action;
    find_action(end_id);
    var end_action=update_action;

    start_action.connections.cout.push(new cout_obj(end_id));
    end_action.connections.cin.push(new cin_obj(start_id));
    }catch(err1){
       // alert(err1);
    }
// alert(new cout_obj(end_id)+"Exit action_update_connect"+new cin_obj(start_id));
}

function action_update_remove_box(eid){
    $(update_state.event.action).each(function(i,act){
        if(act.id == eid){
            array_index= i;
        }
    });
    update_state.event.action.splice(array_index,1);
}

function find_action(id){
    $(update_state.event.action).each(function(i,act){
        if(act.id == id){
            update_action=act;
        }
    });
    
}

function print_json(){
    message_box('JSON',$.toJSON(profile_json));
}

function fn_save_action(){	
    check_orphan_action();
    if(!orphan_action){
        if(!save_action){
            save_now();
            save_action=true;           
        }
        $('#bp_property_1').hide();
        
    }else{
        message_box("Error","Orphan action detected<br>First connect or remove orphan action");
    }
    
    
}

function save_and_close(){
    $("#confirm_dialog").html("Do you want to close?");
    $("#confirm_dialog").dialog('option','title',"Close");
    $("#confirm_dialog").dialog('option','buttons',{
        'Ok':function(){
            $(this).dialog('close');
            check_orphan_action();
            if(orphan_action){
                message_box("Error","Orphan action detected<br>First connect or remove orphan action");
            }
            else{
                reset_canvas();
                update_state=undefined;
                if(!save_action){
                    save_now();
                    save_action=true;
                }

                $('#bp_property_1').hide();
                $("#tab_controlflow").addClass('active').show();
                $('#button_save_state').show();
                $('#button_close_state').show();
                $('#button_save_action').hide();
                $('#button_back_action').hide();
                $("#tab_datasource").hide();

                //$('#upload_pd').hide();
                //$('.run_pdl_button').show();
                

                tab_controlflow_click();
                load_control_diagram(profile_json);
            }

            
        },
        'Cancel':function(){
            $(this).dialog('close');
        },
        'Close without Save':function(){
            $(this).dialog('close');
            reset_canvas();
            update_state=undefined;
            $("#tab_controlflow").addClass('active').show();
            $('#button_save_state').show();
            $('#button_close_state').show();
            $('#button_save_action').hide();
            $('#button_back_action').hide();
            $("#tab_datasource").hide();
            tab_controlflow_click();
            load_control_diagram(profile_json);
            save_action=true;
        }
    });
    $("#confirm_dialog").dialog('open');


    

    
   
   
}

function state_close(){

    $(".run_pdl_button").show();

    $('#bp_property_1').hide();

    $("#confirm_dialog").html("Do you want to close?");

    $("#confirm_dialog").dialog('option','title',"Close");

    $("#confirm_dialog").dialog('option','buttons',{

        'Ok':function(){

            $(this).dialog('close');

            if(save_state){

                save_state=true;

                reset_canvas();

                update_state=undefined;

                //    $('#bp_property_1').hide();

                $('#button_close_state').hide();

                $("#button_save_state").hide();

                $("#tab_controlflow").hide();

                $("#tab_connection").addClass('active').show();

                tab_connection_click();
                $('#upload_pd').show();
                $('.run_pdl_button').hide();
            }

            else{

                message_box('Save','First save your states');

            }

        },

        'Cancel':function(){

            $(this).dialog('close');
            $('.run_pdl_button').show();
        },

        'Close without Save':function(){

            $(this).dialog('close');

            reset_canvas();

            update_state=undefined;

            $('#button_close_state').hide();

            $("#button_save_state").hide();

            $("#tab_controlflow").hide();

            $("#tab_connection").addClass('active').show();

            tab_connection_click();

            save_state=true;
            $('#upload_pd').show();
            $('.run_pdl_button').hide();
        }

    });

    $("#confirm_dialog").dialog('open');      
    
   

}


function check_orphan_state(){
    orphan_state=false;  
    try{
        $(profile_json.process_definition.state).each(function(i,act){
            //	if(act.connections !== undefined){
            if(act.connections.cin.length ==0 || act.connections.cout.length ==0){
                orphan_state= true;  /* changes done to orphan_state = false*/
            }
        // 	}
        //alert(act.id+"   "+act.connections.cin.length+"  "+act.connections.cout.length);
        });
    
    }catch(err4){
      //  alert(err4);
    }
}

function check_orphan_action(){
    orphan_action=false;
    try{
        $(update_state.event.action).each(function(i,act){
    	
            /*   if(act.connections.cin.length ==0 && act.connections.cout.length ==0){
            orphan_action=true;
        }*/
        
            /*      if(act.name.substring(3,0)=='rea' || act.name.substring(3,0)=='sta'){
        if(act.connections.cin.length ==0 && act.connections.cout.length ==0){
            orphan_action=true;
        }
        }
        if(act.name.substring(3,0)=='map' || act.name.substring(3,0)=='wri'){
        if(act.connections.cin.length ==0 || act.connections.cout.length ==0){
            orphan_action=true;
        }
        }
        */
            if(act.name.substring(3,0)=='rea')
            {
                if(act.connections.cin.length ==0 && act.connections.cout.length ==0)
                {
                    orphan_action=true;
                }
            }
            if(act.name.substring(3,0)=='map')
            {
                if(act.connections.cout.length ==0)
                {
                    orphan_action=true;
                }
            }
            if(act.name.substring(3,0)=='wri')
            {
                if(act.connections.cin.length ==0 && act.connections.cout.length ==0)
                {
                    orphan_action=true;
                }
            }
            if(act.name.substring(3,0)=='sta')
            {
                if(act.connections.cin.length ==0 && act.connections.cout.length ==0)
                {
                    orphan_action=true;
                }
            }
        //alert(act.id+"   "+act.connections.cin.length+"  "+act.connections.cout.length);
        });
    }catch(err3){
        alert('err3:'+err3);
    }
}

function pdl_duplicate(new_name){
	
	
    return_val = false;
				
    $('.root  li.doc').each(function(i,j){
					
        if(new_name == $(j).html())
            return_val = true;
					
					
    });
    return return_val;
}
 function message_box2(message){
                $("#tab_message").empty().show().text(message);
                 setTimeout('$("#tab_message").hide("slow").empty()','2000');
  }
 function message_box3(message,className){ 	
                $(className).empty().show("fast").text(message);
               // $(className).before('<img src="images/accept.png"/>');
                 setTimeout('$("'+className+'").hide("slow").empty()','2000');
 }