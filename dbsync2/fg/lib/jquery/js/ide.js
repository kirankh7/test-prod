



action_start = undefined;
action_end = undefined;


function ide_init(){
    
    init_ide();
    init_dialogs();

    $("#create_connection").click(function(){
        $("#popup").dialog('open');
    });
    $("#upload_pd").click(function(){
        $("#popup_upload").dialog('open');
    });
   



    $(window).resize(function(){
        update_page();
    });




    $("#show_hide").click(function(){
        $('#bp_property_1').slideToggle('slow');
    });


    $(".blk").click(function(event){

        alert('connecting'+ event.target);
        if(action_start != undefined){

            window.action_end = $(this).attr('id');
            draw_action_connection(action_start,action_end);
        }
    });


    list_adaptors();
    load_tree();

    $("#button_save_state").click(function(){
        save_now();
    });
    $("#button_save_action").click(function(){
        fn_save_action();
    });
    $("#button_back_action").click(function(){
        save_and_close();
    });

    $("#button_close_state").click(function(){
        state_close();
    });

    $("#wait_dialog").ajaxStart(function() {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            },
            message: 'Please Wait...'//$('#wait_dialog')
        });
    });
    $("#wait_dialog").ajaxSuccess(function() {
        $.unblockUI();
    });
    $("#wait_dialog").ajaxError(function() {
        $.unblockUI();
    });

    $('.drag_create, #create_action').draggable({
        revert:true
    });

    $('#wrap').droppable({
        accept: '.drag_create',
        drop:function(ev, ui){
        	if(name_dataflow($(ui.draggable).attr('rel'))){        		        	
        	//dataflow add new reader/writer/status/map
            add_new_element($(ui.draggable).offset(),$(ui.draggable).attr('rel'));
        	}
        }
    });

    $('#wrap_ctrl').droppable({
        accept: '#create_action',
        drop:function(ev, ui){
            //	var i =0;
            //controlflow add new state
            if(name_pdl()){
                //if(i ==0){                
                add_new_element_control($(ui.draggable).offset(),$(ui.draggable).attr('rel'));
            //	}
            //	i=1;
            }
        }
    });


    $('#cc_2').scroll(function (){
        updateCanvas3($("#canvas"), $(".blk_ctrl"));
    //set_pan();
    });
    $('#cc_3').scroll(function (){
        updateCanvas2($("#canvas"), $(".blk"));
    // set_pan();
    });


    $(window).load(function() {
        window.onbeforeunload = navigateAway;
    });



}

function navigateAway(e){
    var message = "Please Check diagrams are saved",
    e = e || window.event;
    // For IE and Firefox
    if (e) {
        e.returnValue = message;
    }
    // For Safari
    return message;
}

function get_end_action(el){
    
    if(window.action_start != undefined){

        window.action_end = $(el).attr('id');
        alert(window.action_end);
        draw_action_connection(window.action_start,window.action_end,'1');
    }
}

function init_ide(){
    $('#cc_1').show();
    $('#cc_2').hide();
    $('#cc_3').hide();

    $('.actions_tab_1').show();
    $('.actions_tab_2').hide();
    $('.actions_tab_3').hide();
    $('#bp_property_1').show();


    $('#tab_controlflow').hide();
    $('#tab_datasource').hide();

    $('#bp_property_1').hide();
    $('.active_processes').hide();
    $('.console_running_processes').hide();
    $('.stop_pdl_button').hide();
    //hide buttons

    $("#save_adapter_property").hide();
    $('#button_close_state').hide();
    $("#save_property").hide();
    $("#button_save_state").hide();
    $("#button_save_action").hide();
    $("#button_back_action").hide();

    //$('#tab_connection').click(tab_connection_click);

    //$('#tab_controlflow').click(tab_controlflow_click);

    //$('#tab_datasource').click(tab_datasource_click);

    // $("#pan_container").hide();

    $('#upload_pd').show();
    $('.stop_pdl_button').hide();
    $('.run_pdl_button').hide();

    $(".console_op").click(function(){
        $('#bp_property_2').show();
        $("#bp_property_1").hide();
        $('.active_processes').hide();
        $('.console_running_processes').hide();
        //$('.stop_pdl_button').hide();
    });
    $(".active_process").click(function(){
    	 refreshIframe();
    	 $('.active_processes').show();
        $("#bp_property_1").hide();
        $('#bp_property_2').hide();
        $('.console_running_processes').hide();
        
        //$('.stop_pdl_button').hide();
    });
    $(".Console").click(function(){
    	refreshConsoleIframe();
   	 $('.console_running_processes').show();
   	 $('.active_processes').hide();
     $("#bp_property_1").hide();
     $('#bp_property_2').hide();
   });
    
}
function setIframeSrcValue(srcurl){
	//alert('setted......'+srcurl);
	document.getElementById('active_process_console').src = srcurl;
	 $('.console_running_processes').show();
	 $('.active_processes').hide();
     $("#bp_property_1").hide();
     $('#bp_property_2').hide();
}
function init_dialogs(){
    $("#message_dialog").dialog({
        autoOpen:false,
        bgiframe: true,
        modal: true,
        width:'300px',
        resizable:false,
        buttons: {
            Ok: function() {
                $(this).dialog('close');
            }
        }
    });
    $("#prompt_dialog").dialog({
        autoOpen:false,
        bgiframe: true,
        modal: true,
        width:'300px',
        resizable:false
    });

   
    

    $("#confirm_dialog").dialog({
        autoOpen:false,
        bgiframe: true,
        modal: true,
        width:'300px',
        resizable:false
    });

    $("#popup").dialog({
        autoOpen:false,
        bgiframe: true,
        modal: true,
        width:'700px',
        resizable:false,
        title:'Create New Adaptor',
        buttons: {
            Save: function() {
                $(this).dialog('close');
                var adaptername=$("input[name='adapterName']").val();
                $("input[name='adapterName']").val('');
                //  var adaptertype=$("select[name='adapterType']").val();
                //?profileName=<%=model.getProfileName()%>&command=createnew&selectedAdapter=" + name + "&selectedAdapterType=" + adapterType
                //  $.getJSON(adaptor_url+'?action=create&name=aname&type=atype',function(data){
                // name = document.myform.adapterName.value;
              
                //replace starting blank spaces without blank spaces
                adaptername =adaptername.replace(/^\s\s*/, '');
                if (adaptername == null || adaptername == "")
                {
                    message_box('Create Adaptor',"You must enter a name for the new adapter.!");
                    return;
                }
			    if(/[\s]/.test(adaptername)){ //if contains a space        	    
			    	//alert("Adapter name cannot contain spaces.");
			    	message_box('Create Adaptor',"Adapter name cannot contain spaces.");
			    	return;
			    }
                var adapterNames = new Array();
                AdapterNames = document.myform.adapterNamesList.value;
                /*list of adapters for user created already */
                adapterNames = AdapterNames.replace("[","").replace("]","").split(",");
                select = document.myform.adapterType	
                options = select.options;
                index = select.selectedIndex;
                selectedOption = options[index];
                adapterType = selectedOption.value;
                isDuplicate =new Boolean(false);
                isDuplicate = checkDuplicateName(adapterNames,adaptername);
                 var mytime= "&ms="+new Date().getTime();
                if(!isDuplicate)
                {
                    $.get('?profileName='+profileName+'&command=createnew&selectedAdapter='+ adaptername +'&selectedAdapterType='+adapterType+mytime,function(data){
                        message_box('Create Adaptor',"Created Successfully\n");
                        list_adaptors();
                    });
                }
                else{
                    message_box('Create Adaptor',"Already same adapter name exists!");
                    return;
                }
            },
            Cancel:function(){            	
                $(this).dialog('close');
            }
        }
    });
    $("#popup_upload").dialog({
        autoOpen:false,
        bgiframe: true,
        modal: true,
        width:'700px',
        resizable:false,
        title:'Upload PDL',
        buttons: {
            /*          Upload: function() {
                $(this).dialog('close');
             var accountId = document.exportform.accountId.value;
			    var profileName = document.exportform.profileName.value;
			   var filetype = document.getElementById("file");			
				var file  = filetype.files[0];
                var callthis ='uploaddata.m?accountId='+accountId+'&profileName='+profileName+'&file='+file;
                
                formval = document.getElementById('exportform');   
*/
            /*        var callthis = 'uploaddata.m';
               var filetype = document.getElementById("file");			
				var file  = filetype.files[0];
				
				alert("File name: " + file.fileName);
                
                callthis = callthis+"?"+$(formval).serialize()+"&file="+file;
                * */
            /*          alert(callthis);
					 $.post(callthis,function(data){					 							       
					        message_box('Upload PDL',data);			    			
			    });
           },   */
            Cancel:function(){
                load_tree();
                list_adaptors();
                $(this).dialog('close');
            }
        }
    });
}

function checkDuplicateName(adapterNames,createdName){
    var setValue = new Boolean(false);
    for(var i=0;i<adapterNames.length;i++){
        if(createdName.toLowerCase() == adapterNames[i].toLowerCase().ltrim()){
            setValue = true;
            break;
        }else{
            setValue = false;
        }
    }
    return setValue;
}
 
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}
 
function tab_connection_click(){
    $('#cc_1').show();
    $('#cc_2').hide();
    $('#cc_3').hide();

    $('.actions_tab_1').show();
    $('.actions_tab_2').hide();
    $('.actions_tab_3').hide();

    $('#bp_property_1').hide();
    $('#bp_property_2').hide();
    $('#bp_property_3').hide();
    $('.console_running_processes').hide();
    $('.active_processes').hide();
    $('.stop_pdl_button').hide();
    $(this).addClass('active');
    $('#tab_controlflow').removeClass('active');
    $('#tab_datasource').removeClass('active');

    $("#button_save_diagram").hide();
}

String.prototype.endsWith = function(str)
{	
    return (this.match(str+"$")==str)
	
}
String.prototype.startsWith = function(str)
{
    return (this.match("^"+str)==str)
}

function tab_controlflow_click(){
    $('#cc_1').hide();
    $('#cc_2').show();
    $('#cc_3').hide();

    $('.actions_tab_1').hide();
    $('.actions_tab_2').show();
    $('.actions_tab_3').hide();

    $('#bp_property_1').hide();
    $('#bp_property_2').show();
    $('#bp_property_3').hide();
    $('.console_running_processes').hide();
    $('.active_processes').hide();
    $('.stop_pdl_button').hide();
    $(this).addClass('active');
    $('#tab_connection').removeClass('active');
    $('#tab_datasource').removeClass('active');
    updateCanvas3($("#canvas"), $(".blk_ctrl"));

    // set_pan($(this).attr("id"));

    $("#button_save_diagram").show();
    //$("#tab_controlflow span").html(profile_json.process_definition.name);
    display_selected_pdl_name = selected_pdl_name;
   
    if(display_selected_pdl_name.startsWith("processdefinition_")){
        display_selected_pdl_name = display_selected_pdl_name.split("processdefinition_")[1];
    }
     
    if(display_selected_pdl_name.endsWith(".xml")){
        display_selected_pdl_name = display_selected_pdl_name.split(".xml")[0];
    }        
    $("#tab_controlflow span").html(display_selected_pdl_name);
}

function tab_datasource_click(){
    $('#cc_1').hide();
    $('#cc_2').hide();
    $('#cc_3').show();

    $('.actions_tab_1').hide();
    $('.actions_tab_2').hide();
    $('.actions_tab_3').show();

    $('#bp_property_1').hide();
    $('#bp_property_2').hide();
    $('#bp_property_3').show();
    $('.console_running_processes').hide();
    $('.active_processes').hide();
    $('.stop_pdl_button').hide();
    $(this).addClass('active');
    $('#tab_connection').removeClass('active');
    $('#tab_controlflow').removeClass('active');

    updateCanvas2($("#canvas"), $(".blk"));

    // set_pan($(this).attr("id"));

    $("#button_save_diagram").show();
    $("#tab_datasource span").html(update_state.name);
}

function update_panels(){

    position = $("#canvas_content").offset();
    $('.cc').css('top', position.top );
    $('.cc').css('left', position.left );

    //$("#ide_main_panel").width($(document).width() - 180 -194);
    //$("#debug").html($(document).width() - 180 -194);

    //$("#ide_left_panel").width();
    //$("#ide_right_panel").width();

    var side_bar_width =  $("#ide_left_panel").width() + $("#ide_right_panel").width() + 6;


    $("#ide_main_panel").width($(window).width() - side_bar_width);
    //$("#debug").html($(document).width() - side_bar_width+ );



    $('.cc').width($('#canvas_content').width());
    $('#canvas').width($('#canvas_content').width());
}

function gebid(eid){
    return document.getElementById(eid);
}

function update_page(){

    position = $("#canvas_content").offset();
    $('.cc').css('top', position.top );
    $('.cc').css('left', position.left );
    //alert(position.top +' ' +position.left);

    if($(window).width() <= 1018){
        $('body').css('width','1018px');

    }
    else{
        $('body').css('width',$(window).width()+'px');
        
    }

    
    if($.browser.msie){
        //alert('im ie');
       //    $("#ide_main_panel").width($('body').width() - ($("#ide_left_panel").width()+$("#ide_right_panel").width())+21);
        $("#ide_main_panel").width($('body').width() - ($("#ide_left_panel").width()+$("#ide_right_panel").width()));
    }else{
        //alert('im mozilla');
        //$("#ide_main_panel").width($('body').width() - 340);
        $("#ide_main_panel").width($('body').width() - ($("#ide_left_panel").width()+$("#ide_right_panel").width()));

    }
    





    $('.cc').width($('#canvas_content').width());

    $('#canvas').width($('#canvas_content').width());

}



$(document).ready(function(){

     update_page();
    
});