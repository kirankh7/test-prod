



var eid=0;
var ceid=0;
$(document).ready(function(){
    //alert('i am here');
    $('.drag_create, #create_action').draggable({
        revert:true
    });
		
    $('#wrap').droppable({
        accept: '.drag_create',
        drop:function(ev, ui){			
            add_new_element($(ui.draggable).offset(),$(ui.draggable).attr('rel'));			    
        }
    });

    $('#wrap_ctrl').droppable({
        accept: '#create_action',
        drop:function(ev, ui){	
            //alert('dropped');
            add_new_element_control($(ui.draggable).offset(),$(ui.draggable).attr('rel'));			    
        }
    }); 
	
    
    $('#cc_2').scroll(function (){
        updateCanvas3($("#canvas"), $(".blk_ctrl"));
        //
        //          if($(this).scrollTop() > 1000)
        //              $(this).scrollTop(1012);
        //          if($(this).scrollLeft() > 450)
        //              $(this).scrollLeft(468);


        // $('#debug').html('height'+$(this).scrollTop()+'- width'+$(this).scrollLeft()) ;
        set_pan();
    });
    $('#cc_3').scroll(function (){
        updateCanvas2($("#canvas"), $(".blk"));

        //          if($(this).scrollTop() > 1000)
        //              $(this).scrollTop(1012);
        //          if($(this).scrollLeft() > 450)
        //              $(this).scrollLeft(468);



        //$('#debug').html('height:'+$(this).scrollTop()+'- width:'+$(this).scrollLeft()) ;
        set_pan();
    });
    
    
    
});





function add_new_element(pos,type){
    name  = "box_"+ (eid++);  // prompt('Enter Element id');
    if($("#"+name).length) {
        alert('Element already Exists');
    }
    else{
        add_new_box(name,name+'body',pos, type);
    }
}

function add_new_element_control(pos,type){
    name  = "control_"+ (ceid++);  // prompt('Enter Element id');
    if($("#"+name).length) {
        alert('Element already Exists');
    }
    else{
        add_new_box_control(name,name+'body',pos, type);
    }
}





function remove_connection(c){
			
    var start = $(c).parent().parent().parent().parent().attr('id');
    var  end = $(c).parent().attr('rel');
		    
    $(c).parent().remove();
		  
    $.each($("#"+end+" ul.incoming li") , function(i,a){
			
        li = $(a);
        //alert(li.attr("rel")+"-"+start);
        if(li.attr("rel") == start)
            li.remove();
			
    });
		
    updateCanvas2($("#canvas"), $(".blk"));
}













function add_new_box( head , body, pos ,type ){

	
    pos_top = pos.top- $('#canvas').offset().top+$('#cc_3').scrollTop();
    pos_left = pos.left - $('#canvas').offset().left+$('#cc_3').scrollLeft();
	
	
    var div_attributes = {
        'class':'blk ui-widget-content posi' ,
        id: head
    };
    var p_properties = {
        'style':'display:none;' ,
        'class':'type'
    };

	    
	    
    $("#wrap").createPrepend(
        'div', div_attributes,
        [
        // ui-widget-header is for jQuery UI
        'h1', {
        //'class':'ui-widget-header'
        },type+' '+head,
        'p', {}, [
        'ul', {
            'class':'outgoing'
        }, '',
        'ul', {
            'class':'incoming'
        }, '',
        ],
        //'<ul class="outgoing" ></ul><ul class="incoming"/></ul>',
        'p', p_properties, type,

        ])
    .css('position','absolute')
    .css('top',pos_top+'px')
    .css('left',pos_left+'px')
    .css('z-index','9999')
    .contextMenu({
        menu: 'myMenu'
    },
    function(action, el, pos) {
        //alert(action);
        if( action == 'connect')
            connect($(el).attr('id'));
        else if ( action == 'remove'){
            remove_box($(el).attr('id'));
        }
    }
    );



    $(".blk").draggable({
        handle: 'h1',
        containment: '#wrap' ,
        //scroll:	false,
        drag: function() {

            updateCanvas2($("#canvas"), $(".blk"));
        //save_position(this);
        }
        
    });
}

function add_new_box_control( head , body, pos ,type ){

    pos_top = pos.top- $('#canvas').offset().top+$('#cc_2').scrollTop();
    pos_left = pos.left - $('#canvas').offset().left+$('#cc_2').scrollLeft();
	
	
    var div_attributes = {
        'class':'blk_ctrl ui-widget-content posi' ,
        id: head
    };
    var p_properties = {
        'style':'display:none;' ,
        'class':'type'
    };

	    
	    
    $("#wrap_ctrl").createPrepend(
        'div', div_attributes,
        [
        // ui-widget-header is for jQuery UI
        'h1', {
        //'class':'ui-widget-header'
        },type+' '+head,
        'p', {'style':'width:100px;' }, [
        'ul', {
            'class':'outgoing'
        }, '',
        'ul', {
            'class':'incoming'
        }, '',
        ],
        //'<ul class="outgoing" ></ul><ul class="incoming"/></ul>',
        'p', p_properties, type,

        ])
    .css('position','absolute')
    .css('top',pos_top+'px')
    .css('left',pos_left+'px')
    .css('z-index','9999')
    .contextMenu({
        menu: 'myMenu'
    },
    function(action, el, pos) {
        //alert(action);
        if( action == 'connect')
            connect2($(el).attr('id'));
        else if ( action == 'remove'){
            remove_box($(el).attr('id'));
        }
    }
    );


    $(".blk_ctrl").draggable({
        handle: 'h1',
        containment: '#wrap_ctrl' ,
        //scroll:	false,
        drag: function() {

            updateCanvas2($("#canvas"), $(".blk_ctrl"));
        }
       
    //save_position(this);
        
    });
}





function connect(id){
    $('#'+id).addClass('sl');
    var start=id;
    var end=0;
    var connect = 1;
    
    
    $('.blk').click(function(){
	    
        end=$(this).attr('id');
	    
        if(start!=0 && end!=0){
	    
            var limit_out = outgoing_limit(start);
	
            var limit_in = incomming_limit(end);
	    
	
	
	    
            var outgoing_connects = outgoing_connections(start);
	    
            var incomming_connects = incomming_connections(end);
	    
            //alert(limit_out.connections+' - '+ outgoing_connects);
	
            if( outgoing_connects >= limit_out.connections){
				
                alert(limit_out.message);
                start=0;
                end=0;
                connect=0;
                $('.blk').removeClass('hv');
                $('.blk').removeClass('sl');
		
            }
            if( incomming_connects >= limit_in.connections){
				
                alert(limit_in.message);
                start=0;
                end=0;
                connect=0;
                $('.blk').removeClass('hv');
                $('.blk').removeClass('sl');
		
            }
	    
	    
	
            //$(this).hide().fadeIn('slow');

		
            if(start!=0 && end!=0){
                if(start!= end){
			
                    if(check_preconnect(start,end) == 0){
				
                        $('#'+start +' >p ul.outgoing').append('<li rel = '+end+'> Connect to '+end+'<a class="key" onclick="remove_connection(this)">[x]</a></li>');
                        $('#'+end +' >p ul.incoming').append('<li rel = '+start+'> Connect from '+start+'</li>');
                        updateCanvas2($("#canvas"), $(".blk"));
                        start=0;
                        end=0;
                        connect=0;
                        $('.blk').removeClass('hv');
                        $('.blk').removeClass('sl');
                    }
                    else{
                        alert('Connection already exits');
                        start=0;
                        end=0;
                        connect=0;
                        $('.blk').removeClass('hv');
                        $('.blk').removeClass('sl');
                    }
                }
                else{
                    alert('Cannot connect to same');
                    start=0;
                    end=0;
                    connect=0;
                    $('.blk').removeClass('hv');
                    $('.blk').removeClass('sl');
			
                }
            }

        }
        
        
    });
    


}


function incomming_limit(element){
	
	
    type = $("#"+element+" p.type").html();
    //alert(type);
    switch(type)
    {
        case	'reader':
            return {
            'connections': '0' ,
            'message': 'You cannot connect to a Reader'
        };
					
        case	'map':
            return {
            'connections': '1' ,
            'message': 'Map can only have 1 Input'
        };
					
        case	'writer':
            return{
            'connections': '1',
            'message': 'Writer can only have 1 Intput'
        };
					
        case	'status':
            return {
            'connections': '1',
            'message': 'Writer can only have 1 Intput'
        };
					
		
        default:
            return {
            'connections': '-1',
            'message': 'Error!!'
        };
					
    }
	
	
}


function outgoing_limit(element){
	
	
    type = $("#"+element+" p.type").html();
    //alert(type);
    switch(type)
    {
        case	'reader':
            return	{
            'connections': '20',
            'message': 'You cannot connect a Reader to more than 10 Maps'
        };
        case	'map':
            return	{
            'connections': '1',
            'message': 'Map can only have 1 Output'
        };
        case	'writer':
            return	{
            'connections': '1',
            'message': 'Writer can only have 1 Output'
        };
        case	'status':
            return	{
            'connections': '0',
            'message': 'Status Writer cannot have an output'
        };
        default:
            return	{
            'connections': '-1',
            'message':' Error!!'
        };
    }
	
	
}

//Checks if the connection already exits from start to end
function check_preconnect(start,end)
{
    var error = 0;
		
    $.each($("#"+end+" ul.incoming li") , function(i,a){
			
        li = $(a);
        //alert(li.attr("rel")+"-"+start);
        if(li.attr("rel") == start)
            error = -1;
			
    });
		
		
    return error;
}

//gets the number of Outgoing connections
function outgoing_connections(element){
	
    return $("#"+element+" ul.outgoing li").length;
	
	
	
}
//gets the number of Incomming connections
function incomming_connections(element){
	
    return $("#"+element+" ul.incoming li").length;
	
	
}

//removes and element
//TODO: Function should be altered
function remove_box(element){
	
    var message = '';
	
    if((outgoing_connections(element) == 0) && (incomming_connections(element) == 0 ) ){
        $("#"+element).remove();
        reset_canvas($("#canvas"));
    }
    else{
        alert('Cannot Remove Element \n Please Remove all Incoming and Outgoing Connections to this Element ');
    }
		
		
		
}

	
	
//called to connect between elements in Controll TAB
function connect2(id){
    $('#'+id).addClass('sl');
    var start=id;
    var end=0;
    var connect = 1;

        
    
    $('.blk_ctrl').click(function(){
	    
        end=$(this).attr('id');
	    
        if(start!=0 && end!=0){
	    
           
		    
            var outgoing_connects = outgoing_connections(start);
	    
            var incomming_connects = incomming_connections(end);
	    
            //alert(limit_out.connections+' - '+ outgoing_connects);
	
            if( outgoing_connects >= 1){
				
                alert('Controlls can only Have 1 Output');
                start=0;
                end=0;
                connect=0;
                $('.blk_ctrl').removeClass('hv');
                $('.blk_ctrl').removeClass('sl');
		
            }
            if( incomming_connects >= 1){
				
                alert('Controlls can only have 1 input ');
                start=0;
                end=0;
                connect=0;
                $('.blk_ctrl').removeClass('hv');
                $('.blk_ctrl').removeClass('sl');
		
            }
	
	
		
            if(start!=0 && end!=0){
                if(start!= end){
			
                    if(check_preconnect(start,end) == 0){
				
                        $('#'+start +' >p ul.outgoing').append('<li rel = '+end+'> Connect to '+end+'<a class="key" onclick="remove_connection(this)">[x]</a></li>');
                        $('#'+end +' >p ul.incoming').append('<li rel = '+start+'> Connect from '+start+'</li>');
                        updateCanvas2($("#canvas"), $(".blk_ctrl"));
                        start=0;
                        end=0;
                        connect=0;
                        $('.blk_ctrl').removeClass('hv');
                        $('.blk_ctrl').removeClass('sl');
                    }
                    else{
                        alert('Connection already exits');
                        start=0;
                        end=0;
                        connect=0;
                        $('.blk_ctrl').removeClass('hv');
                        $('.blk_ctrl').removeClass('sl');
                    }
                }
                else{
                    alert('Cannot connect to same');
                    start=0;
                    end=0;
                    connect=0;
                    $('.blk_ctrl').removeClass('hv');
                    $('.blk_ctrl').removeClass('sl');
			
                }
            }

        }
        
        
    });
    


}





function pan_canvas()
{
    var active_tab = $("#tabs li.active").attr("id");
    var pan_container_offset = $("#pan_container").offset();
    var pan_controller_offset = $("#pan_controller").offset();

    pan_x =  pan_controller_offset.left - pan_container_offset.left;
    pan_y = pan_controller_offset.top - pan_container_offset.top  ;

    //TODO Generate ratios dynamically

    pan_x *= 3.25; 
    pan_y *= 11.38;


    //$("#debug").html("Tab:"+active_tab+"Offsets<br/>"+pan_x+"px <br/>"+pan_y);

    if(active_tab == "tab_controlflow")
    {
        //cc_2
        $("#cc_2").scrollTop(pan_y);
        $("#cc_2").scrollLeft(pan_x);
    }
    else if(active_tab == "tab_datasource")
    {
        //cc_3
        $("#cc_3").scrollTop(pan_y);
        $("#cc_3").scrollLeft(pan_x);
    }


}

function set_pan(tab)
{
    var pan_container_offset = $("#pan_container").offset();
    //var pan_controller_offset = $("#pan_controller").offset();
    tab =  $("#tabs li.active").attr("id");
    
    // $("#debug").html("Setting pan");
    if( tab == "tab_datasource")
    {
        pan_x =  $("#cc_3").scrollLeft();
        pan_y = $("#cc_3").scrollTop();


    }
    else if( tab == "tab_controlflow")
    {
        pan_x =  $("#cc_2").scrollLeft();
        pan_y = $("#cc_2").scrollTop();
    }

    pan_x /= 3.25; //TODO: generate ratios dynamically
    pan_y /= 11.38;

    // $("#debug").html(pan_x+"-"+pan_y);

    //        $("#pan_controller").css("left",pan_container_offset.left + pan_x);
    //        $("#pan_controller").css("top",pan_container_offset.top + pan_y);
        
    $("#pan_controller").css("left", pan_x);
    $("#pan_controller").css("top", pan_y);

//     pan_x =  pan_controller_offset.left - pan_container_offset.left;
//     pan_y = pan_controller_offset.top - pan_container_offset.top;
}