var xmlHttp;
var formName;

/* Array Extensions */
Array.prototype.contains = function(obj) {
  var i = this.length;
  while (i--) {
    if (this[i] === obj) {
      return i;
    }
  }
  return -1;
}
Array.prototype.remove = function(from, to) {
  var rest = this.slice((to || from) + 1 || this.length);
  this.length = from < 0 ? this.length + from : from;
  return this.push.apply(this, rest);
};
/*   */

function initpage(){
	dragDrop.initElement('query');
}

function getProperty(soption, state,type,_formName)
{ 
	//alert("actionId:::"+actionId);
	//alert(soption.options[soption.selectedIndex].value);
	section = soption.options[soption.selectedIndex].value;
	var mytime= "&ms="+new Date().getTime();
	//alert(type +"pdl_name::::");
	//alert(section);
	xmlHttp=GetXmlHttpObject();
	if (xmlHttp==null)
  	{
  		alert ("Your browser does not support AJAX!");
  		return;
  	} 
  	formName = _formName;  	
	var url="pdl.m";
	//url=url+"?command=getproperty&pdl=<%=model.getPdl()%>&profileName=<%=model.getProfile().name %>";
	url=url+"?command=getproperty";
	url=url+"&profileName="+uname;
	url=url+"&pdl="+pdl;
	url=url+"&state="+state;
	url=url+"&section="+section;
	url=url+"&sectionType="+type;
	url=url+"&id="+id+mytime;
	//alert(_formName);
	xmlHttp.onreadystatechange=updateSection;
	xmlHttp.open("GET",url,true);
	xmlHttp.send(null);
}

function updateSection() 
{ 
	if (xmlHttp.readyState==4)
	{ 
		
	//	alert(document.getElementById("div_"+formName).innerHTML);
	//	alert(xmlHttp.responseText);
	//	document.getElementById("div_display").innerHTML=xmlHttp.responseText;	
	 var displayForm = "div_"+formName;
	// alert(displayForm+"--------displayForm");
	document.getElementById(displayForm).innerHTML=xmlHttp.responseText;
	}
}

function GetXmlHttpObject()
{
	var xmlHttp=null;
	try
	  {
	  // Firefox, Opera 8.0+, Safari
	  xmlHttp=new XMLHttpRequest();
	  }
	catch (e)
	  {
	  // Internet Explorer
	  try
	    {
	    xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
	    }
	  catch (e)
	    {
	    xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
	    }
	  }
	  //alert(xmlHttp);
	return xmlHttp;
}

function updateView(el,cls){
	//alert(cls+""+laststate);
	/*if (el.className==('activity_expanded '+cls)){
		el.className='activity_shrink ' + cls;
		document.getElementById(el.id+'_detail').style.display='none';
	} else {
		el.className='activity_expanded '+cls;
		document.getElementById(el.id+'_detail').style.display='block';
	}*/
	//alert(laststate+'---'+el+':close');
	//$('#bp_property_1').hide();
	if (laststate==el.id+':close'){
		el.className='activity_shrink ' + cls;
		document.getElementById(el.id+'_detail').style.display='none';
		laststate='';
		
	} else if (el.className==('activity_shrink '+cls)){
		el.className='activity_expanded ' + cls;
	//	alert(el.className+"^^^^^^^^^^^^^^^^^^^^");
	//	alert("#####################"+el.id);
		document.getElementById(el.id+'_detail').style.display='block';
	} 
	
}

laststate="";

queryInputFieldId="";


function closeBlock(elid, cls){
	
	//el = document.getElementById(elid);
	//alert(el+'-'+cls);
	$('#bp_property_1').hide();
	//alert(el.className);
	/*if (el.className==('activity_expanded '+cls)){
		
		laststate=elid+':close';
		el.className='activity_shrink ' + cls;
		document.getElementById(elid+'_detail').style.display='none';
		
		//alert('x');
	} 
	* */
}

function deleteBlock(id){
	
	var confirmOnceBeforeDelete = confirm('Do you really want to delete?');
	if(confirmOnceBeforeDelete == true){	
	document.theform.command.value="delete";
	document.theform.id.value=id;
	document.theform.submit();
	}
	else{
	}
}

function createBlock (blockname, parentId, id){
	document.theform.command.value="create";
	document.theform.create.value=blockname;
	document.theform.parentId.value=parentId;
	document.theform.id.value=id;
	document.theform.submit();
}

function showStatusMap(adapter,elementId,map){
	//alert("COOOL");
	//alert(adapter+"------"+elementId+"-------"+map);
	//elementId = elementId;
	document.theform.action="map.m";
	document.theform.command.value="";
	document.theform.id.value=elementId;
	document.theform.adapterName.value=adapter;
	document.theform.map.value=map;
	document.theform.target="_new";
	document.theform.submit();
}

var lookuptype;

function buildquery(formid, queryid, _type){
	//alert(formid+"----"+queryid+"----"+_type);
	var mytime= "&ms="+new Date().getTime();
	$(window).scrollTo($(document).height(),500);
	lookuptype=_type;
	// get the selected adapter		
	selectOption = document.getElementById(formid);
	//adapter = section;
	adapter = selectOption.options[selectOption.selectedIndex].value;	
	//alert(document.getElementById(queryid).innerHTML);
	var text = document.getElementById(queryid);	
	var query = text.value;	
	document.getElementById('querytree').innerHTML="Please wait while we retrieve the tables...";
	document.getElementById('query').style.visibility='visible';
	
	queryInputFieldId = queryid;
	// init values
	tables = new Array();
	fields = new Array();
	// 
	xmlHttp=GetXmlHttpObject();
	if (xmlHttp==null)
  	{
  		alert ("Your browser does not support AJAX!");
  		return;
  	} 
	var url="getadaptermetadata.m";
	url=url+"?command=table&pdl="+selected_pdl_name+"&profileName="+profileName+"&adapterName="+adapter+"&sourceQuery="+query+mytime;
	//alert(url);
	xmlHttp.onreadystatechange=showQueryBuilder;
	xmlHttp.open("GET",url,true);
	xmlHttp.send(null);
}


function showQueryBuilder(){
	if (xmlHttp.readyState==4)
	{ 
		//alert(document.getElementById("div_"+formName).innerHTML);
		//alert(xmlHttp.responseText);
		document.getElementById('querytree').innerHTML=xmlHttp.responseText;
		
		//convertTrees();
	}
}
var queryTable=null;

function getFields(adapter, lisection){
	if (lisection.className=='liOpen')
		return;
	lisection.className='liOpen';
	queryTable=lisection.id;
	
	xmlHttp=GetXmlHttpObject();
	if (xmlHttp==null)
  	{
  		alert ("Your browser does not support AJAX!");
  		return;
  	} 
	var mytime= "&ms="+new Date().getTime();
	var url="getadaptermetadata.m";
	url=url+"?command=field&pdl=<%=model.getPdl()%>&profileName="+profileName+"&adapterName="+adapter+"&sourceQuery="+queryTable+mytime;

	xmlHttp.onreadystatechange=expandTable;
	xmlHttp.open("GET",url,true);
	xmlHttp.send(null);
	
}

function expandTable(){
	if (xmlHttp.readyState==4)
	{ 
		//alert(document.getElementById("div_"+formName).innerHTML);
		//alert(xmlHttp.responseText);
		
		document.getElementById(queryTable+'_fields').innerHTML=xmlHttp.responseText;
		//convertTrees();
	}
}

function closediv(divid){
	//alert(document.getElementById(divid).style.visibility);
	document.getElementById(divid).style.visibility='hidden';
	queryInputFieldId="";
	$(window).scrollTo($(document).height(),500);
	
}

var tables = new Array();
var fields = new Array();

function updateTable(checkbox, table){
	//alert(table+':'+checkbox.checked);
	if (checkbox.checked==true){
		tables.push(table);
	} else {
		index=tables.contains(table);
		if (index>=0){
			tables.remove(index);
		}
	}
	updateQuery(tables);
}

function updateField(checkbox, field){
	if (checkbox.checked==true){
		fields.push(field);
	} else {
		index=fields.contains(field);
		if (index>=0){
			fields.remove(index);
		}
	}
	updateQuery(fields);
}

function updateQuery(){
	if (lookuptype=='query'){
		/*alert(adapter+'Adapter Name');
		alert(adapterType+'Adapter Type');
		if(adapterType=='com.avankia.appmashups.adapter.intacct.IntacctAdapter'){
			document.getElementById(queryInputFieldId).value = 'select_list '+ fields.toString()+ ' from '+ tables.toString();
		}else{*/
			document.getElementById(queryInputFieldId).value = 'select '+ fields.toString()+ ' from '+ tables.toString();
		//}
	} else if (lookuptype=='table'){
		document.getElementById(queryInputFieldId).value = tables.toString();
	} else if (lookuptype=='field'){
		document.getElementById(queryInputFieldId).value = fields.toString();
	}
}

/* Mouse move for div Ref: http://www.quirksmode.org/js/dragdrop.html
 */

 dragDrop = {
			keyHTML: '<a href="#" class="keyLink"><!--#--></a>',
			keySpeed: 10, // pixels per keypress event
			initialMouseX: undefined,
			initialMouseY: undefined,
			startX: undefined,
			startY: undefined,
			dXKeys: undefined,
			dYKeys: undefined,
			draggedObject: undefined,
			initElement: function (element) {
				if (typeof element == 'string')
					element = document.getElementById(element);
				element.onmousedown = dragDrop.startDragMouse;
				element.innerHTML += dragDrop.keyHTML;
				var links = element.getElementsByTagName('a');
				var lastLink = links[links.length-1];
				lastLink.relatedElement = element;
				lastLink.onclick = dragDrop.startDragKeys;
			},
			startDragMouse: function (e) {
				if (e.target.nodeName!='DIV'){// this is to avoid scrolling when dragged on the scrollbar
					dragDrop.startDrag(this);
					var evt = e || window.event;
					dragDrop.initialMouseX = evt.clientX;
					dragDrop.initialMouseY = evt.clientY;
					addEventSimple(document,'mousemove',dragDrop.dragMouse);
					addEventSimple(document,'mouseup',dragDrop.releaseElement);
				}
				return false;
			},
			startDragKeys: function () {
				dragDrop.startDrag(this.relatedElement);
				dragDrop.dXKeys = dragDrop.dYKeys = 0;
				addEventSimple(document,'keydown',dragDrop.dragKeys);
				addEventSimple(document,'keypress',dragDrop.switchKeyEvents);
				this.blur();
				return false;
			},
			startDrag: function (obj) {
				if (dragDrop.draggedObject)
					dragDrop.releaseElement();
				dragDrop.startX = obj.offsetLeft;
				dragDrop.startY = obj.offsetTop;
				dragDrop.draggedObject = obj;
				obj.className += ' dragged';
			},
			dragMouse: function (e) {
				var evt = e || window.event;
				var dX = evt.clientX - dragDrop.initialMouseX;
				var dY = evt.clientY - dragDrop.initialMouseY;
				dragDrop.setPosition(dX,dY);
				return false;
			},
			dragKeys: function(e) {
				var evt = e || window.event;
				var key = evt.keyCode;
				switch (key) {
					case 37:	// left
					case 63234:
						dragDrop.dXKeys -= dragDrop.keySpeed;
						break;
					case 38:	// up
					case 63232:
						dragDrop.dYKeys -= dragDrop.keySpeed;
						break;
					case 39:	// right
					case 63235:
						dragDrop.dXKeys += dragDrop.keySpeed;
						break;
					case 40:	// down
					case 63233:
						dragDrop.dYKeys += dragDrop.keySpeed;
						break;
					case 13: 	// enter
					case 27: 	// escape
						dragDrop.releaseElement();
						return false;
					default:
						return true;
				}
				dragDrop.setPosition(dragDrop.dXKeys,dragDrop.dYKeys);
				if (evt.preventDefault)
					evt.preventDefault();
				return false;
			},
			setPosition: function (dx,dy) {
				dragDrop.draggedObject.style.left = dragDrop.startX + dx + 'px';
				dragDrop.draggedObject.style.top = dragDrop.startY + dy + 'px';
			},
			switchKeyEvents: function () {
				// for Opera and Safari 1.3
				removeEventSimple(document,'keydown',dragDrop.dragKeys);
				removeEventSimple(document,'keypress',dragDrop.switchKeyEvents);
				addEventSimple(document,'keypress',dragDrop.dragKeys);
			},
			releaseElement: function() {
				removeEventSimple(document,'mousemove',dragDrop.dragMouse);
				removeEventSimple(document,'mouseup',dragDrop.releaseElement);
				removeEventSimple(document,'keypress',dragDrop.dragKeys);
				removeEventSimple(document,'keypress',dragDrop.switchKeyEvents);
				removeEventSimple(document,'keydown',dragDrop.dragKeys);
				dragDrop.draggedObject.className = dragDrop.draggedObject.className.replace(/dragged/,'');
				dragDrop.draggedObject = null;
			}
		}



function addEventSimple(obj,evt,fn) {
	if (obj.addEventListener)
		obj.addEventListener(evt,fn,false);
	else if (obj.attachEvent)
		obj.attachEvent('on'+evt,fn);
}

function removeEventSimple(obj,evt,fn) {
	if (obj.removeEventListener)
		obj.removeEventListener(evt,fn,false);
	else if (obj.detachEvent)
		obj.detachEvent('on'+evt,fn);
}
function limitText(limitField, limitNum) {
    if (limitField.value.length > limitNum) {
        limitField.value = limitField.value.substring(0, limitNum);
    } 
}

