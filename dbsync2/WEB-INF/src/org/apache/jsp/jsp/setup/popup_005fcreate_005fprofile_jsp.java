/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: JspCServletContext/1.0
 * Generated at: 2014-04-18 18:30:23 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp.jsp.setup;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.*;
import com.avankia.appmashups.web.*;
import java.text.SimpleDateFormat;
import com.avankia.appmashups.web.ProfileSetupModel;

public final class popup_005fcreate_005fprofile_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n \r\n\r\n\r\n\r\n");

	ProfileSetupModel model = (ProfileSetupModel)request.getAttribute("model");

      out.write("\r\n\r\n<div id=\"popup\">\r\n\t\t<!--  <div class=\"overlay_title\">Add/Edit Connection -Title Changable</div>-->\r\n\t\t<div class=\"overlay_blue_title\">Create New Profile</div>\r\n\t\t<div class=\"overlay_body\">\r\n\t\t<form name=\"frmMain\" method=\"post\" action=\"profilesetup.m\">\r\n\t\t<input type=\"hidden\" name=\"profileName\" value=\"\"/>\r\n\t\t    <table>\r\n\t\t        <tr>\r\n\t\t            <td>Name</td>\r\n\t\t            <td><input type=\"text\" name=\"profName\" /></td>\r\n\t\t            <td></td>\r\n\t            </tr>\r\n\t");
      out.write("\r\n\t\t        <tr>\r\n\t\t            <td></td>\r\n\t\t            <td></td>\r\n\t\t            <td> \r\n\t\t            <button onclick=\"createProfile('");
      out.print(model.getProfileList());
      out.write("');\">Save</button> \r\n\t\t           <button onclick=\"javascript:window.close();\">Cancel</button>\r\n\t\t           </td>\r\n\t            </tr>\r\n\t\t    </table>\r\n\t\t    </form>\r\n\t\t</div>\r\n\t</div>");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
