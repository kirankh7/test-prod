/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: JspCServletContext/1.0
 * Generated at: 2014-04-18 18:30:22 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp.jsp.setup;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import com.avankia.appmashups.logging.*;
import com.avankia.appmashups.engine.conversion.ProcessMonitor.DBSyncProcess;
import java.util.*;

public final class consoleviewer_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n\r\n");

	ConsoleViewerModel model = (ConsoleViewerModel) request.getAttribute("model");
	//List<String> messages = model.getConsoleMessages();
	List<DBSyncProcess> dbProcesses = model.getDbsyncProcess();
	List<String> inActiveLogFileList = model.getInactiveProcessLogFileList();
	long timer = 999999999;
	if(dbProcesses!=null &&  dbProcesses.size()>0){
		timer = 10;
	}

      out.write("\r\n<meta http-equiv=\"refresh\" content=");
      out.print(timer);
      out.write(">\r\n<style type=\"text/css\">\r\n.colheader {\r\n\ttext-align: left;\r\n\tfont-size: 12px;\r\n\tfont-weight: bold;\r\n\tline-height: 146%;\r\n\tcolor: #444444;\r\n\tfont-family: 'Arial Bold', Arial, Helvetica, sans-serif;\r\n\tmargin-top: 5px;\r\n}\r\n\r\n.u255 {\r\n\theight: 3px;\r\n\twidth: 990px;\r\n}\r\n\r\n.u255_line {\r\n\theight: 3px;\r\n\twidth: 100%;\r\n}\r\n\r\n.u255_line {\r\n\tbackground-image: url(\"images/u255_line.png\");\r\n}\r\n\r\n.u36_container {\r\n\theight: 20px;\r\n\twidth: 250px;\r\n\tcursor: pointer;\r\n}\r\n\r\n#u36_img {\r\n\theight: 26px;\r\n\twidth: 136px;\r\n}\r\n\r\n.u36_original {\r\n\tbackground-image: url(\"images/transparent.gif\");\r\n}\r\n\r\n.u37 {\r\n\tfont-family: Arial;\r\n\theight: 15px;\r\n\tposition: absolute;\r\n\ttext-align: left;\r\n\twidth: 126px;\r\n\tword-wrap: break-word;\r\n}\r\n\r\n.u37 {\r\n\tfont-family: Arial;\r\n\ttext-align: left;\r\n\tword-wrap: break-word;\r\n}\r\n\r\n.u38_original {\r\n\tbackground-image: url(\"images/u38_original.png\");\r\n}\r\n\r\n.u251 {\r\n\tfont-family: Arial;\r\n\theight: 16px;\r\n\ttext-align: left;\r\n\twidth: 168px;\r\n\tword-wrap: break-word;\r\n\tcolor: #6B6B6B;\r\n}\r\n\r\n.Txt_Style {\r\n\tcolor: #0066FF;\r\n");
      out.write("\tfloat: left;\r\n\tfont-family: 'Arial Bold', Arial, Helvetica, sans-serif;\r\n\tfont-size: 70%;\r\n\tfont-weight: bold;\r\n\tline-height: 160%;\r\n\tmargin-bottom: 0;\r\n\tpadding-bottom: 2px;\r\n\tpadding-top: 1px;\r\n\ttext-align: left;\r\n\t/*width: 49px;*/\r\n}\r\n.Txt_Style2 {\r\n\tcolor: #0066FF;\r\n\tfloat: left;\r\n\tfont-family: 'Arial Bold', Arial, Helvetica, sans-serif;\r\n\tfont-size: 80%;\r\n\tfont-weight: bold;\r\n\tline-height: 160%;\r\n\tmargin-bottom: 0;\r\n\tpadding-bottom: 2px;\r\n\tpadding-top: 1px;\r\n\ttext-align: left;\r\n\t/*width: 49px;*/\r\n}\r\n\r\n#img_icon {\r\n\tfloat: left;\r\n\theight: 13px;\r\n\tmargin-bottom: 0;\r\n\tmargin-right: 5px;\r\n\tmargin-top: 4px;\r\n\twidth: 12px;\r\n}\r\n\r\n.Txt_Style p {\r\n\tmargin: 0px;\r\n}\r\n.Txt_Style2 p {\r\n\tmargin: 0px;\r\n}\r\n.action_holder {\r\n\tfloat: left;\r\n\ttext-align: center;\r\n\twidth: 35px;\r\n}\r\n\r\n.action_holder img {\r\n\tfloat: left;\r\n\tmargin-left: 10px;\r\n}\r\n\r\n.txt_action {\r\n\tcolor: #0066FF;\r\n\tfont-family: Arial, Helvetica, sans-serif;\r\n\tfont-size: 75%;\r\n\tmargin-bottom: 0;\r\n\tpadding-bottom: 2px;\r\n\tpadding-top: 1px;\r\n\ttext-align: left;\r\n\twidth: 27px;\r\n");
      out.write("}\r\n.txt_content{\r\n    color: #444444;\r\n    font-family: Arial,Helvetica,sans-serif;\r\n    font-size: 75%;\r\n    line-height: 160%;\r\n    margin-bottom: 0;\r\n    padding-bottom: 2px;\r\n    padding-top: 1px;\r\n    text-align: left;\r\n}\r\n.blk{\r\n\tpadding-left: 10px;\r\n\tfloat: left;\r\n}\r\n\r\n</style>\r\n<script>\r\n/*function killProcessUrl(url){\r\n \talert('stop refresh console done' +url);\r\n \t//window.open(url,\"_self\");\r\n \twindow.location.href=url;\r\n \tparent.refreshIframe();\r\n\t//var iframe = document.getElementById('active_process_console');\r\n\t//iframe.src = iframe.src;\r\n}*/\r\n\r\n\r\n</script>\r\n\r\n<div class=\"tabProcess\">\r\n\t<!-- <span class=\"refresh\">Refresh</span> -->\r\n\t<div id=\"u36_container\" class=\"u36_container\">\r\n\t\t<img id=\"img_icon\" alt=\"refresh\" src=\"images/refresh_icon.png\" />\r\n\t\t<!-- <div class=\"Txt_Style\" onclick=\"window.location.reload()\"> -->\r\n\t\t<!-- <div class=\"Txt_Style\" onClick=\"window.location.href=window.location.href\"> -->\r\n\t\t<div class=\"Txt_Style\" onClick=\"javascript:parent.refreshIframe()\">\r\n\t\t\r\n\t\t\t<p class=\"lastNode\">Refresh</p>\r\n");
      out.write("\t\t</div>\r\n\t\t<div class=\"blk\">\r\n\t\t\t<img id=\"img_icon\" alt=\"delete\" src=\"images/delete.png\" />\r\n\t\t\t<!--  onClick=\"javascript:deleteLogs('consolelogviewer.m?profileName=");
      out.print(model.getProfileName());
      out.write("&command=delete')\"-->\r\n\t\t\t<div class=\"Txt_Style2\" >\r\n\t\t\t\t \r\n\t\t\t\t\t<p class=\"lastNode\"><a class=\"Txt_Style2\" href=\"consolelogviewer.m?profileName=");
      out.print(model.getProfileName());
      out.write("&command=delete\" style=\"text-decoration: none;\">Delete Inactive Logs</a> </p>\r\n\t\t\t\t \r\n\t\t\t</div>\r\n\t\t\t\r\n\t\t</div>\r\n\t</div>\r\n\t<!-- <div id=\"u36_container\" class=\"u36_container\">\r\n\t\t\r\n\t</div> -->\r\n\t<!-- <div class=\"title\">Running Processes</div> -->\r\n\t<table width=\"100%\">\r\n\t\t<tr>\r\n\t\t\t<div id=\"u255_line\" class=\"u255_line\"></div>\r\n\t\t\t<img id=\"u255\" class=\"u255\" src=\"images/transparent.gif\" />\r\n\t\t\t<th class=\"colheader\">Process/Project</th>\r\n\t\t\t<th class=\"colheader\">Running Time</th>\r\n\t\t\t<th class=\"colheader\">Actions</th>\r\n\t\t\t\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td colspan=\"3\">\r\n\t\t\t\t<div id=\"u255_line\" class=\"u255_line\"></div> <img id=\"u255\"\r\n\t\t\t\tclass=\"u255\" src=\"images/transparent.gif\" />\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t\t");
 // we have to reverse the list
		for (DBSyncProcess dbp : dbProcesses) { 
		
      out.write("\r\n\r\n\t\t<tr>\r\n\t\t\t<td class=\"row\">\r\n\t\t\t\t<span class=\"txt_style\">\r\n\t\t\t\t\t");
      out.print(dbp.getConversionProcessor().getPdlFileName().substring(dbp.getConversionProcessor().getPdlFileName().indexOf("_") + 1, dbp.getConversionProcessor().getPdlFileName().indexOf(".")));
      out.write("\r\n\t\t\t\t</span>\r\n\t\t\t</td>\r\n\t\t\t<td>");
try{ 
      out.write("\r\n\t\t\t\t<span class=\"txt_content\">");
      out.print(model.convertMilliSecondsToDate(dbp.getSession().split("__")[1]));
      out.write("</span>\r\n\t\t\t\t");
}catch(Exception e){} 
      out.write("\r\n\t\t\t</td>\r\n\t\t\t<td>\r\n\t\t\t\t<div class=\"action_holder\">\r\n\t\t\t\t\t<img src=\"images/kill_icon.png\" alt=\"\" height=\"\" style=\"float: left; margin-top: 1px;\"/> \r\n\t\t\t\t\t<a href=\"consolelogviewer.m?command=kill&runId=");
      out.print(dbp.getSession());
      out.write("\" style=\"text-decoration: none;\"><span class=\"txt_action\">Kill</span>\r\n\t\t\t\t\t</a>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"action_holder\">\r\n\t\t\t\t\t<img src=\"images/log_icon.png\" alt=\"\" height=\"\"/> \r\n\t\t\t\t\t\r\n\t\t\t\t\t<a href=\"\" class=\"console_running_processes\" onclick=\"parent.setIframeSrcValue('consolelogviewer.m?command=viewlog&runId=");
      out.print(dbp.getSession());
      out.write("')\"\r\n\t\t\t\t\t\tstyle=\"text-decoration: none;\"><span class=\"txt_action\">log</span>\r\n\t\t\t\t\t</a>\r\n\t\t\t\t</div>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td colspan=\"3\">\r\n\t\t\t\t<div id=\"u255_line\" class=\"u255_line\"></div> <img id=\"u255\"\r\n\t\t\t\tclass=\"u255\" src=\"images/transparent.gif\">\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t\t");
 } for (String logFile : inActiveLogFileList) { 
      out.write("\r\n\t\t<tr>\r\n\t\t\t<td class=\"row\">\r\n\t\t\t\t<span class=\"txt_style\">");
      out.print(logFile.split("__")[0]);
      out.write("</span>\r\n\t\t\t</td>\r\n\t\t\t<td>");
try{ 
      out.write("\r\n\t\t\t\t<span class=\"txt_content\">");
      out.print(model.convertMilliSecondsToDate(logFile.split("__")[1]));
      out.write("</span>\r\n\t\t\t\t");
}catch(Exception e){} 
      out.write("\r\n\t\t\t</td>\r\n\t\t\t<td>\r\n\t\t\t\t<div class=\"action_holder\">\r\n\t\t\t\t\t<img src=\"images/log_icon.png\" alt=\"\" height=\"\"/> \r\n\t\t\t\t\t\t<a href=\"logviewer.m?profileName=");
      out.print(model.getProfileName());
      out.write("&command=list&logType=file&currentLogFile=");
      out.print(logFile);
      out.write("\" class=\"console_running_processes\" id=\"");
      out.print(logFile);
      out.write("\" target=\"new\"\r\n\t\t\t\t\t\tstyle=\"text-decoration: none;\">\r\n\t\t\t\t\t\t<span class=\"txt_action\">log </span></a>\r\n\t\t\t\t</div>\r\n\t\t\t</td>\r\n\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td colspan=\"3\">\r\n\t\t\t\t<div id=\"u255_line\" class=\"u255_line\"></div> <img id=\"u255\" class=\"u255\" src=\"images/transparent.gif\">\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t\t");
 } 
      out.write("\r\n\t</table>\r\n</div>\r\n\r\n");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
