/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: JspCServletContext/1.0
 * Generated at: 2014-04-18 18:30:22 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp.jsp.setup;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import com.avankia.appmashups.engine.conversion.*;
import com.avankia.appmashups.engine.conversion.adapters.*;
import org.jdom.*;
import java.util.*;
import com.avankia.appmashups.web.*;
import com.avankia.appmashups.engine.*;
import org.jdom.*;
import org.jdom.xpath.*;

public final class editprocess2_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {


String getName(Element el, String xpath) throws Exception{
	Object obj = null;
	if ((obj=XPath.selectSingleNode(el,xpath))==null){
		return "Not Set";
	}
	Element xpel = (Element)obj;//XPath.selectSingleNode(el,xpath);
	if (el!=null){
		return xpel.getAttributeValue("name");
	}
	return "N/A";
}

String adaptersToSelect(Profile profile, String selectName, String selectValue, String state, String type, String formId){
	StringBuffer buf = new StringBuffer();
	buf.append("<select name=\""+selectName+"\" onchange=\"javascript:getProperty(this,'"+state+"','"+type+"','"+formId+"')\" >");
	HashMap adapterMap = profile.adapters;
	Set set = adapterMap.keySet();
	Iterator itr = set.iterator();
	while (itr.hasNext()){
		String adapterName = (String) itr.next();
		buf.append("<option value=\""+adapterName+ "\""+(adapterName.equalsIgnoreCase(selectValue)?" selected=\"true\"":"")+     " >"+adapterName+"</option>");
	}
	buf.append("<option value=\"ConsoleAdapter\""+("ConsoleAdapter".equalsIgnoreCase(selectValue)?" selected='true'":"")+">ConsoleAdapter</option>");
	buf.append("<option value=\"NullAdapter\""+("NullAdapter".equalsIgnoreCase(selectValue)?" selected='true'":"")+">NullAdapter</option>");
	buf.append("</select>");
	
	return buf.toString();
}

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');

	PDLModel model =  (PDLModel)request.getAttribute("model");
	Element pdlElement = model.getPdlElement();

      out.write("\r\n\r\n<script language=\"javascript\">\r\nhelpurl=\"pdlPage\";\r\nvar xmlHttp;\r\nvar state;\r\n\r\nfunction showState(_id)\r\n{ \r\n\t//alert(_state);\r\n\txmlHttp=GetXmlHttpObject();\r\n\tif (xmlHttp==null)\r\n  \t{\r\n  \t\talert (\"Your browser does not support AJAX!\");\r\n  \t\treturn;\r\n  \t} \r\n  \t//state = _state;\r\n  \t\r\n\tvar url=\"pdl.m\";\r\n\turl=url+\"?command=showstate&pdl=");
      out.print(model.getPdl());
      out.write("&profileName=");
      out.print(model.getProfile().name );
      out.write("\";\r\n\turl=url+\"&id=\"+_id;\r\n\turl=url+\"&t=\"+new Date().getTime();\r\n\t//alert(url);\r\n\t\r\n\tdocument.getElementById(\"details\").src=url;\r\n\t//xmlHttp.onreadystatechange=updateDetail;\r\n\t//xmlHttp.open(\"GET\",url,true);\r\n\t//xmlHttp.send(null);\r\n\t\r\n}\r\nfunction saveActiveStatus(_id,isActiveChecked){\r\n\txmlHttp = GetXmlHttpObject();\r\n\tif(xmlHttp == null){\r\n\talert(\"Your browser does not support AJAX!\");\r\n\t}\r\n\tvar url=\"pdl.m\";\r\n\turl = url+\"?command=saveActiveState&pdl=");
      out.print(model.getPdl());
      out.write("&profileName=");
      out.print(model.getProfile().name);
      out.write("&isChecked=\"+isActiveChecked;\r\n\turl=url+\"&id=\"+_id;\r\n\turl=url+\"&t=\"+new Date().getTime();\t\r\n\tdocument.getElementById(\"details\").src=url;\r\n\t}\r\nfunction updateDetail() \r\n{ \r\n\tif (xmlHttp.readyState==4)\r\n\t{ \r\n\t\t//alert(document.getElementById(\"div_\"+formName).innerHTML);\r\n\t\t//alert(xmlHttp.responseText);\r\n\t\t//document.getElementById(\"div_\"+formName).innerHTML=xmlHttp.responseText;\r\n\t\t\r\n\t\tdocument.getElementById(\"detailname\").innerHTML = state;\r\n\t\t\r\n\t\tdocument.getElementById(\"details\").innerHTML = xmlHttp.responseText;\r\n\t\t\r\n\t}\r\n}\r\n\r\n\r\nfunction GetXmlHttpObject()\r\n{\r\n\tvar xmlHttp=null;\r\n\ttry\r\n\t  {\r\n\t  // Firefox, Opera 8.0+, Safari\r\n\t  xmlHttp=new XMLHttpRequest();\r\n\t  }\r\n\tcatch (e)\r\n\t  {\r\n\t  // Internet Explorer\r\n\t  try\r\n\t    {\r\n\t    xmlHttp=new ActiveXObject(\"Msxml2.XMLHTTP\");\r\n\t    }\r\n\t  catch (e)\r\n\t    {\r\n\t    xmlHttp=new ActiveXObject(\"Microsoft.XMLHTTP\");\r\n\t    }\r\n\t  }\r\n\treturn xmlHttp;\r\n}\r\n\r\nfunction confirmActionPDL(urlValues){\r\n\tvar msg = 'Do you really want to delete?';\r\n\tvar confirmOnce = confirm(msg);\r\n");
      out.write("\tif(confirmOnce==true){\t\r\n\twindow.location = urlValues;\r\n\t}else{\t\r\n\t}\r\n}\r\n</script>\r\n<script type=\"text/javascript\">\r\nfunction submitCreateAction(){\r\n\tif(document.createpdlform.state.value==\"\"){\r\n\t   alert(\"Please specify the name of the 'Action'\");\r\n\t}else{\r\n\t \tdocument.createpdlform.submit();\r\n\t}\r\n}\r\n\t\r\n\t\r\n</script>\r\n\r\n<script>\r\n/* \r\nwindow.history.forward(1); \r\ndocument.attachEvent(\"onkeydown\", my_onkeydown_handler); \r\nfunction my_onkeydown_handler() \r\n{ \r\nswitch (event.keyCode) \r\n{ \r\n\r\ncase 116 : // 'F5' \r\nevent.returnValue = false; \r\nevent.keyCode = 0; \r\nalert(\"We have disabled F5\"); \r\nbreak; \r\n} \r\n} */\r\n</script>\r\n\t\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\r\n<form name=\"createpdlform\" action=\"pdl.m\" >\r\n\t<table width=\"100%\" >\r\n\t\t<tr>\r\n\t\t\t<td>\r\n\t\t\t\t<a href=\"dbsyncprofiledefinition.m?profileName=");
      out.print(model.getProfile().name );
      out.write("\">&lt;&lt; back to process list</a>\r\n\t\t\t</td>\r\n\t\t\t<td align=\"right\" style=\"padding-right: 5px\">\r\n\t\t\t\t<input type=\"text\" name=\"state\" value=\"\"/>&nbsp; <a href=\"#\" onClick=\"javascript:submitCreateAction();\">Create Action</a>\r\n\t\t\t\t<input type=\"hidden\" name=\"command\" value=\"createaction\"/>\r\n\t\t\t\t<input type=\"hidden\" name=\"profileName\" value=\"");
      out.print(model.getProfile().name );
      out.write("\"/>\r\n\t\t\t\t<input type=\"hidden\" name=\"pdl\" value=\"");
      out.print(model.getPdl() );
      out.write("\"/>\r\n\t\t\t</td>\r\n\t\t\t<td align=\"right\" style=\"padding-right: 5px\">\r\n\t\t\t\t<a href=\"runpdl.m?profileName=");
      out.print(model.getProfile().name);
      out.write("&processFileName=");
      out.print(model.getPdl());
      out.write("&command=start\" target=\"_new\">Run</a>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t </table>\r\n</form>\r\n\r\n\t\t\t\t\t\t\r\n<table width=\"100%\" style=\"border: 1px solid #cccccc;\" >\r\n<tr>\r\n\t<td width=\"200px\" valign=\"top\" align=\"center\">\r\n\t\t<table width=\"100%\" border=\"0\" cellspacing=\"2\">\r\n\t\t");

			List stateElementList = XPath.selectNodes(pdlElement,"/process-definition/state");
			System.out.println(stateElementList.size());
			int formId=0;
			
			for (int i=0;i<stateElementList.size();i++){
				Element stateElement = (Element)stateElementList.get(i);
				String state = stateElement.getAttributeValue("name");
				
      out.write("\r\n\t\t\t\t<!-- tr \t\t\t\t \r\n\t\t\t\tstyle=\"background-color:#ffffff\" \r\n\t\t\t\t\tonmouseover=\"this.style.backgroundColor='#FCED88'\" \r\n\t\t\t\t\tonmouseout=\"this.style.backgroundColor='#ffffff'\"\r\n\t\t\t\t \r\n\t\t\t\t\t\r\n\t\t\t\t\t<td>");
      out.print(i+1 );
      out.write(".\r\n\t\t\t\t\t</td>\r\n\t\t\t\t\t<td onclick=\"javascript:showState('");
      out.print(stateElement.getAttributeValue("id"));
      out.write("')\">\r\n\t\t\t\t\t\t<table width=\"100%\" class=\"blank\">\r\n\t\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t\t<td>\r\n\t\t\t\t\t\t<b>");
      out.print(stateElement.getAttributeValue("name"));
      out.write("</b><br/>\r\n\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t<tr><td align=\"right\">\r\n\t\t\t\t\t\t<a href=\"javascript:showState('");
      out.print(stateElement.getAttributeValue("id"));
      out.write("')\">select</a> &nbsp;&nbsp;\r\n\t\t\t\t\t\t<a href=\"pdl.m?profileName=");
      out.print(model.getProfile().name );
      out.write("&pdl=");
      out.print(model.getPdl() );
      out.write("&command=deleteaction&id=");
      out.print(stateElement.getAttributeValue("id"));
      out.write("\">delete</a>\r\n\t\t\t\t\t\t</td></tr>\r\n\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t</td>\r\n\t\t\t\t</tr -->\r\n\t\t\t\t<tr style=\"background-color:#ffffff\" \r\n\t\t\t\tonmouseover=\"this.style.backgroundColor='#FCED88'\" \r\n\t\t\t\t\tonmouseout=\"this.style.backgroundColor='#ffffff'\"\r\n\t\t\t\t>\r\n\t\t\t\t\t<td colspan=\"2\">\r\n\t\t\t\t\t\t<div class=\"ui-accordion ui-widget ui-helper-reset\">\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<h3 class=\"ui-accordion-header ui-helper-reset ui-state-active ui-corner-top\">\r\n\t\t\t\t\t\t\t\t<span class=\"ui-icon ui-icon-triangle-1-e\"></span>\r\n\t\t\t\t\t\t\t\t<a href=\"javascript:showState('");
      out.print(stateElement.getAttributeValue("id"));
      out.write("')\">");
      out.print(i+1);
      out.write('.');
      out.write(' ');
      out.print(stateElement.getAttributeValue("name"));
      out.write("</a>\r\n\t\t\t\t\t\t\t\t</h3>\r\n\t\t\t\t\t\t\t\t<div class=\"ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content-active\">\r\n\t\t\t\t\t\t\t\t");

									if(stateElement.getAttribute("isActive") == null){
      out.write("\r\n\t\t\t\t\t\t\t\t\t\t&nbsp;&nbsp;<input type=\"checkbox\"   id=\"activestatus-");
      out.print(stateElement.getAttributeValue("name"));
      out.write("\" onchange=\"javascript:saveActiveStatus('");
      out.print(stateElement.getAttributeValue("id"));
      out.write("',this.checked);\">\r\n\t\t\t\t\t\t\t\t\t");
}else{
									if(stateElement.getAttributeValue("isActive").equals("true")){
      out.write("\r\n\t\t\t\t\t\t\t\t\t&nbsp;&nbsp;<input type=\"checkbox\"  checked=\"");
      out.print(stateElement.getAttributeValue("isActive"));
      out.write("\" id=\"activestatus-");
      out.print(stateElement.getAttributeValue("name"));
      out.write("\" onchange=\"javascript:saveActiveStatus('");
      out.print(stateElement.getAttributeValue("id"));
      out.write("',this.checked);\">\r\n\t\t\t\t\t\t\t\t\t");
 }else{
      out.write("\r\n\t\t\t\t\t\t\t\t\t&nbsp;&nbsp;<input type=\"checkbox\"  id=\"activestatus-");
      out.print(stateElement.getAttributeValue("name"));
      out.write("\" onchange=\"javascript:saveActiveStatus('");
      out.print(stateElement.getAttributeValue("id"));
      out.write("',this.checked);\">\r\n\t\t\t\t\t\t\t\t\t");
 }
									}
									
      out.write("\t&nbsp;&nbsp;active\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t<table width=\"100%\">\r\n\t\t\t\t\t\t\t\t\t<tr><td align=\"right\">\r\n\t\t\t\t\t\t\t\t\t<a href=\"javascript:showState('");
      out.print(stateElement.getAttributeValue("id"));
      out.write("')\">select</a> &nbsp;&nbsp;\r\n\t\t\t\t\t\t\t\t\t");
if(model.getUserAuth().pdlEditable()){
      out.write("\r\n\t\t\t\t\t\t\t\t\t<a href=\"javascript:confirmActionPDL('pdl.m?profileName=");
      out.print(model.getProfile().name);
      out.write("&pdl=");
      out.print(model.getPdl());
      out.write("&command=deleteaction&id=");
      out.print(stateElement.getAttributeValue("id"));
      out.write("')\">delete</a>\r\n\t\t\t\t\t\t\t\t\t");
}else{
      out.write("\r\n\t\t\t\t\t\t\t\t\t\t<font color='gray' size='2px'>delete</font>\r\n\t\t\t\t\t\t\t\t\t");
}
      out.write("\r\n\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t</td></tr>\r\n\t\t\t\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t</td>\r\n\t\t\t\t</tr>\r\n\t\t\t\t");
				
			}
		
      out.write("\r\n\t\t</table>\r\n\t</td>\r\n\t<td align=\"left\" valign=\"top\">\r\n\t\t<iframe id=\"details\" class=\"sectionframe123\" src=\"\" name=\"details\" height=\"500\" width=\"100%\" border=\"0\" frameborder=\"0\" scrolling=\"yes\" >\r\n\t\t</iframe>\r\n\t</td>\r\n</tr>\r\n</table>\r\n<script>\r\n var showFirstAction = \"");
      out.print(model.isShowFirstAction());
      out.write("\";\r\n var actionId = \"");
      out.print(model.getId());
      out.write("\";\r\n if(showFirstAction==\"true\" && actionId != \"null\"){\r\n\t showState(actionId);\r\n }\r\n</script>\r\n\r\n");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
