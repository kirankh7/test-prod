/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: JspCServletContext/1.0
 * Generated at: 2014-04-18 18:30:24 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp.jsp.intacct.qb;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.*;
import com.avankia.appmashups.intacct.*;
import com.avankia.appmashups.intacct.qbimporter.*;

public final class showaccountmap_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n\r\n    \r\n");

	QBAccountMapModel model = (QBAccountMapModel)request.getAttribute("model");
	List<GLAccount> list = model.glList;
	


      out.write("\r\n<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\r\n<html>\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">\r\n<title>DBSync 2</title>\r\n<link href=\"/a/css/dbsync2.css\" rel=\"stylesheet\" type=\"text/css\">\r\n<link href=\"css/dbsync2.css\" rel=\"stylesheet\" type=\"text/css\">\r\n\r\n<script language=\"javascript\">\r\n\r\nfunction goback(){\r\n\t\r\n\tdocument.mapForm.action=\"qbimport.m?command=migrate&accountId=");
      out.print(model.getAccountId());
      out.write("\";\r\n\tdocument.mapForm.submit();\r\n}\r\n\r\n</script>\r\n</head>\r\n<body>\r\n<form name=\"mapForm\" action=\"\" method=\"post\">\r\n\r\n<table width=\"100%\">\r\n\t<tr>\r\n\t\t<td>\r\n\t\t<h2>QuickBooks to Intacct Import Utility<h2>\r\n\t\t<div class=\"headerBar\">Map Accounts</div>\r\n\t\t<h3>Step 1: Chart Of Account </h3>\r\n\t\t<table width=\"700px\">\r\n\t\t<tr>\r\n\t\t\t\t<th>\r\n\t\t\t\t\tChart of Account\r\n\t\t\t\t</th>\r\n\t\t\t\t<th>\r\n\t\t\t\t\tIntacct Account Number\r\n\t\t\t\t</th>\r\n\t\t\t\t<th>\r\n\t\t\t\t\tQuickBooks Account Number\r\n\t\t\t\t</th>\r\n\t\t\t\t\r\n\t\t\t</tr>\r\n\t\t");

			for (int i=0;i<list.size();i++){
				GLAccount acct = list.get(i);
		
      out.write("\r\n\t\t\t<tr>\r\n\t\t\t\t<td>\r\n\t\t\t\t\t");
      out.print(acct.title);
      out.write("\r\n\t\t\t\t</td>\r\n\t\t\t\t<td>\r\n\t\t\t\t\t<input type=\"text\" name=\"newAccount\" value=\"\"/>\r\n\t\t\t\t</td>\r\n\t\t\t\t<td>\r\n\t\t\t\t\t<input type=\"text\" name=\"oldAccount\" value=\"");
      out.print(acct.externalid);
      out.write("\"/>\r\n\t\t\t\t</td>\r\n\t\t\t\t\r\n\t\t\t</tr>\r\n\t\t");

			}
		
      out.write("\r\n\t\t\t<tr>\r\n\t\t\t\t<td colspan=\"3\" align=\"right\">\r\n\t\t\t\t\t<input type=\"button\" name=\"button\" value=\"Save\"/> &nbsp;\r\n\t\t\t\t\t<input type=\"button\" name=\"button\" value=\"Cancel\" onclick=\"javascript:goback();\"/>\r\n\t\t\t\t</td>\r\n\t\t\t\t\r\n\t\t\t</tr>\r\n\t\t</table>\r\n\t\t</td>\r\n\t</tr>\r\n</table>\r\n\r\n</form>\r\n</body>\r\n</html>");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
