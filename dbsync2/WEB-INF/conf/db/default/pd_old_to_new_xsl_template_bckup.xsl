<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xml:output method ="xml" omit-xml-declaration="yes" indent="yes"/>
     <xsl:template match="process-definition">
         <!--root element template-->
        <process_definition>
           <xsl:element name="start_state">
                <xsl:attribute name="id">startid</xsl:attribute>
                <transition to="execute" />
                <xsl:apply-templates select="start-state/connections"/>
                <xsl:apply-templates select="start-state/coordinates"/>
           </xsl:element>

           <!-- state element templat.
           ***condtion to count all that have child elements / desendents in state element ***-->
            <xsl:if test="count(./state) &gt; 0">
                <xsl:for-each select="./state">
                     <xsl:element name="state">
                                <xsl:if test="not(@name = '')">
                                <xsl:attribute name="name">
                                    <xsl:value-of select="@name"/>
                                </xsl:attribute>
                                </xsl:if>
                                <xsl:if test="not(@isActive = '')">
                                <xsl:attribute name="isActive">
                                    <xsl:value-of select="@isActive"/>
                                </xsl:attribute>
                                </xsl:if>
                                 <xsl:if test="not(@id = '')">
                                <xsl:attribute name="id">
                                    <xsl:value-of select="@id"/>
                                </xsl:attribute>
                                </xsl:if>
                            <!--state connection element.loop start -->
                             <xsl:for-each select="connections">
                                <connections>
                                      <xsl:for-each select="cout">
                                           <xsl:element name="cout">
                                               <xsl:if test="not(@id = '')">
                                                 <xsl:attribute name="id"> <xsl:value-of select="@id"/></xsl:attribute>
                                                 </xsl:if>
                                            </xsl:element>
                                        </xsl:for-each>
                                         <xsl:for-each select="cin">
                                                <xsl:element name="cin">
                                                     <xsl:if test="not(@id = '')">
                                                     <xsl:attribute name="id"> <xsl:value-of select="@id"/></xsl:attribute>
                                                     </xsl:if>
                                                </xsl:element>
                                        </xsl:for-each>
                                </connections>
                            </xsl:for-each>
                             <!--state connection element. loop end -->
                              <!--state coordinates element. loop start -->
                            <xsl:for-each select="coordinates">
                                <xsl:element name="coordinates">
                                     <xsl:if test="not(@left = '')">
                                     <xsl:attribute name="left">
                                        <xsl:value-of select="@left"/>                                        
                                    </xsl:attribute>
                                    </xsl:if>
                                     <xsl:if test="not(@z = '')">
                                    <xsl:attribute name="z">                                        
                                        <xsl:value-of select="@z"/>                                        
                                    </xsl:attribute>
                                    </xsl:if>
                                    <xsl:if test="not(@top = '')">
                                    <xsl:attribute name="top">                                        
                                        <xsl:value-of select="@top"/>                                        
                                   </xsl:attribute>
                                   </xsl:if>
                               </xsl:element>
                            </xsl:for-each>
                             <!--state coordinates element. loop end  -->
                                <!--child element-->
                                 <xsl:for-each select="event">
                                    <xsl:element name="event">                                       
                                        <xsl:attribute name="type">
                                            <xsl:if test="not(@type = '')">
                                            <xsl:value-of select="@type"/>
                                            </xsl:if>
                                        </xsl:attribute>                                       
                                   </xsl:element>
                                 </xsl:for-each>
                    </xsl:element>
                </xsl:for-each>
          </xsl:if>
          <!-- end of state element -->
           <xsl:element name="end_state">
                <xsl:attribute name="id">endid</xsl:attribute>
                <xsl:apply-templates select="end-state/connections"/>
                <xsl:apply-templates select="end-state/coordinates"/>
            </xsl:element>
        </process_definition>
         <!--end of root element template-->
    </xsl:template>
     <!--start_state/connections element template-->
     <xsl:template match="start-state/connections">
         <connections>
                    <xsl:for-each select="cout">
                      
                           <xsl:element name="cout">
                                <xsl:if test="not(@id = '')">
                                 <xsl:attribute name="id"> <xsl:value-of select="@id"/></xsl:attribute>
                                 </xsl:if>
                            </xsl:element>
                         
                    </xsl:for-each>
                     <xsl:for-each select="cin">
                          

                            <xsl:element name="cin">
                                <xsl:if test="not(@id = '')">
                                 <xsl:attribute name="id"> <xsl:value-of select="@id"/></xsl:attribute>
                                 </xsl:if>
                            </xsl:element>
                           
                     </xsl:for-each>
        </connections>
     </xsl:template>
      <!--end of start_state/connections element template-->
      <!--start_state/coordinates & connection element template-->
     <xsl:template match="start-state/coordinates">
        <xsl:element name="coordinates">
           <xsl:attribute name="left">
                <xsl:value-of select="@left"/>
            </xsl:attribute>
             <xsl:attribute name="z">
                <xsl:value-of select="@z"/>
            </xsl:attribute>
            <xsl:attribute name="top">
                <xsl:value-of select="@top"/>
           </xsl:attribute>
       </xsl:element>
     </xsl:template>
     <xsl:template match="end-state/connections">
             <connections>
                      <xsl:for-each select="cout">

                           <xsl:element name="cout">
                                <xsl:if test="not(@id = '')">
                                 <xsl:attribute name="id"> <xsl:value-of select="@id"/></xsl:attribute>
                                 </xsl:if>
                            </xsl:element>
                    </xsl:for-each>
                     <xsl:for-each select="cin">
                            <xsl:element name="cin">
                                 <xsl:attribute name="id"> <xsl:value-of select="@id"/></xsl:attribute>
                            </xsl:element>
                    </xsl:for-each>
            </connections>
     </xsl:template>
      <!--end of start_state/coordinates & connection element template-->
       <!--end_state/coordinates element template-->
     <xsl:template match="end-state/coordinates">
        <xsl:element name="coordinates">
           <xsl:attribute name="left">
                <xsl:value-of select="@left"/>
            </xsl:attribute>
            <xsl:attribute name="z">
                <xsl:value-of select="@z"/>
            </xsl:attribute>
            <xsl:attribute name="top">
                <xsl:value-of select="@top"/>
           </xsl:attribute>
       </xsl:element>
     </xsl:template>
      <!--end_state/coordinates element template-->
     <xsl:template match="state">
          <xsl:if test="count(./state) &gt; 0">
                <xsl:for-each select="./state">
                     <xsl:element name="state">
                                <xsl:attribute name="name">
                                    <xsl:value-of select="@name"/>
                                </xsl:attribute>
                                <xsl:attribute name="isActive">
                                    <xsl:value-of select="@isActive"/>
                                </xsl:attribute>
                                <xsl:attribute name="id">
                                    <xsl:value-of select="@id"/>
                                </xsl:attribute>
                                <xsl:element name="event">
                                    <xsl:attribute name="type">node_enter</xsl:attribute>
                                    <xsl:apply-templates select="state/coordinates"/>
                               </xsl:element>
                    </xsl:element>
                </xsl:for-each>
          </xsl:if>
      </xsl:template>
</xsl:stylesheet>