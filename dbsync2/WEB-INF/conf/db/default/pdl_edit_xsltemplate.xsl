<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:strip-space elements="*"/>
    <xml:output method ="xml" omit-xml-declaration="yes" indent="yes"/>
     <xsl:template match="process_definition">
         <!--root element template-->
        <process-definition>
            <xsl:attribute name="name">
                <xsl:value-of select="name"/>
            </xsl:attribute>
            <xsl:attribute name="version">
               <xsl:text>2_1</xsl:text>
            </xsl:attribute>
           <xsl:element name="start-state">
                <xsl:attribute name="id">startid</xsl:attribute>
                <xsl:attribute name="name">start_state</xsl:attribute>
                <transition to="execute" />
                <xsl:apply-templates select="start_state/connections"/>
                <xsl:apply-templates select="start_state/coordinates"/>
           </xsl:element>

           <!-- state element templat.
           ***condtion to count all that have child elements / desendents in state element ***-->
            <xsl:if test="count(./state) &gt; 0">
                <xsl:for-each select="./state">
                     <xsl:element name="state">
                                <xsl:attribute name="name">
                                    <xsl:value-of select="name"/>
                                </xsl:attribute>
                                <xsl:attribute name="isActive">
                                    <xsl:value-of select="isActive"/>
                                </xsl:attribute>
                                <xsl:attribute name="id">
                                    <xsl:value-of select="id"/>
                                </xsl:attribute>
                            <!--state connection element.loop start -->
                             <xsl:for-each select="connections">
                                <connections>
                                      <xsl:for-each select="cout">
                                           <xsl:element name="cout">
                                                <xsl:if test="id !=''">
                                                 <xsl:attribute name="id"> <xsl:value-of select="id"/></xsl:attribute>
                                                 </xsl:if>
                                            </xsl:element>
                                      </xsl:for-each>
                                     <xsl:for-each select="cin">
                                            <xsl:element name="cin">
                                                <xsl:if test="id !=''">
                                                 <xsl:attribute name="id"> <xsl:value-of select="id"/></xsl:attribute>
                                                 </xsl:if>
                                            </xsl:element>
                                     </xsl:for-each>
                                </connections>
                            </xsl:for-each>
                             <!--state connection element. loop end -->
                              <!--state coordinates element. loop start -->
                            <xsl:for-each select="coordinates">
                                <xsl:element name="coordinates">
                                   <xsl:attribute name="left">
                                        <xsl:value-of select="left"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="z">
                                        <xsl:value-of select="z"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="top">
                                        <xsl:value-of select="top"/>
                                   </xsl:attribute>
                               </xsl:element>
                            </xsl:for-each>
                             <!--state coordinates element. loop end  -->
                                <!--child element-->
                                  <!--  <xsl:element name="event">
                                        <xsl:attribute name="type">node-enter</xsl:attribute>
                                   </xsl:element>
                                   -->
                                   <xsl:for-each select="event">
                                        <xsl:element name="event">
                                            <xsl:attribute name="type">
                                                <xsl:if test="type != ''">
                                                <xsl:value-of select="type"/>
                                                </xsl:if>
                                            </xsl:attribute>
                                             <xsl:element name="action">
                                           <xsl:for-each select="action">
                                       <!--       <xsl:element name="action"> -->

                                                <xsl:if test="id !=''">
                                                   <xsl:if test="type = 'reader' or type='writer' or  type='map' ">
                                                     <xsl:param name="typename"><xsl:value-of select="type"/></xsl:param>
                                                     <xsl:element name="{$typename}">
                                                             <xsl:attribute name="id"><xsl:value-of select="id" /></xsl:attribute>
                                                             <xsl:attribute name="name"><xsl:value-of select="name" /></xsl:attribute>
                                                             <xsl:attribute name="displayname"><xsl:value-of select="displayname"/></xsl:attribute>
                                                     <!--          <xsl:choose>
                                                                <xsl:when test="$typename = 'map'">
                                                                    <xsl:attribute name="type"><xsl:text>mapped2</xsl:text></xsl:attribute>
                                                               </xsl:when>
                                                             </xsl:choose>-->
                                                   <!--
                                                    <xsl:apply-templates select="reader/coordinates"></xsl:apply-templates>
                                                        <xsl:apply-templates select="state/event/action/coordinates"></xsl:apply-templates>
                                                       <xsl:apply-templates select="state/event/action/connections"></xsl:apply-templates>
                                                        <xsl:with-param name="color" select="state/event/action/connections"/>
                                                   -->
                                                    <xsl:if test="count(coordinates)!= 0">
                                                         <xsl:element name="coordinates">
                                                                   <xsl:attribute name="left">
                                                                      <xsl:value-of select="coordinates/left"/>
                                                                   </xsl:attribute>
                                                                   <xsl:attribute name="z">
                                                                       <xsl:value-of select="coordinates/z"/>
                                                                   </xsl:attribute>
                                                                   <xsl:attribute name="top">
                                                                       <xsl:value-of select="coordinates/top"/>
                                                                   </xsl:attribute>
                                                         </xsl:element>
                                                     </xsl:if>

                                                     <xsl:if test="count(connections)!= 0">
                                                            <xsl:element name="connections">
                                                                  <xsl:for-each select="connections/cout">
                                                                     <xsl:element name="cout">
                                                                            <xsl:if test="id != ''">
                                                                               <xsl:attribute name="id">
                                                                                  <xsl:value-of select="id"/>
                                                                               </xsl:attribute>
                                                                             </xsl:if>
                                                                     </xsl:element>
                                                                 </xsl:for-each>
                                                                 <xsl:for-each select="connections/cin">
                                                                     <xsl:element name="cin">
                                                                            <xsl:if test="id != ''">
                                                                               <xsl:attribute name="id">
                                                                                  <xsl:value-of select="id"/>
                                                                               </xsl:attribute>
                                                                             </xsl:if>
                                                                     </xsl:element>
                                                                 </xsl:for-each>
                                                             </xsl:element>
                                                     </xsl:if>


                                                       </xsl:element>
                                                   </xsl:if>

                                                   <xsl:if test="type = 'status'">
                                                         <xsl:param name="statustag"><xsl:value-of select="type"/></xsl:param>
                                                         <xsl:element name="{$statustag}">
                                                                 <xsl:attribute name="id"><xsl:value-of select="id" /></xsl:attribute>
                                                                 <xsl:attribute name="name"><xsl:value-of select="name" /></xsl:attribute>
                                                                  <xsl:attribute name="displayname"><xsl:value-of select="displayname"/></xsl:attribute>
                                                                 <xsl:element name="onsuccess">
                                                                      <xsl:param name="statusid"><xsl:value-of select="id"/></xsl:param>
                                                                        <xsl:element name="map" >
                                                                            <xsl:attribute name="type">mapped2</xsl:attribute>
                                                                            <xsl:attribute name="id"> <xsl:value-of select="id"/></xsl:attribute>
                                                                            <xsl:value-of  select="id"/>
                                                                        </xsl:element>
                                                                 </xsl:element>
                                                          </xsl:element>
                                                   </xsl:if>
                                                </xsl:if>
                                                   <!-- </xsl:element> -->
                                            </xsl:for-each>
                                            <xsl:for-each select="task">
                                               <xsl:element name="task">
                                                     <xsl:attribute name="id"><xsl:value-of select="@id" /></xsl:attribute>
                                                     <xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
                                                   
                                                     <xsl:for-each select="action">
                                                         <xsl:if test="id !=''">
                                                               <xsl:if test="type='writer' or type='map' ">
                                                                 <xsl:param name="actionname"><xsl:value-of select="type"/></xsl:param>
                                                                 <xsl:element name="{$actionname}">
                                                                 <xsl:attribute name="id"><xsl:value-of select="id" /></xsl:attribute>
                                                                 <xsl:attribute name="name"><xsl:value-of select="name" /></xsl:attribute>
                                                                 <xsl:attribute name="displayname"><xsl:value-of select="displayname"/></xsl:attribute>
                                                                  <xsl:choose>
                                                                <xsl:when test="$actionname = 'map'">
                                                                    <xsl:attribute name="type"><xsl:text>mapped2</xsl:text></xsl:attribute>
                                                               </xsl:when>
                                                             </xsl:choose>
                                                          <!--      <xsl:if test="$actionname = 'map'">
                                                                     <xsl:attribute name="type"><xsl:text>mapped2</xsl:text></xsl:attribute>
                                                                </xsl:if> -->
                                                             
                                                                
                                                                  <xsl:if test="count(coordinates)!= 0">
                                                                     <xsl:element name="coordinates">
                                                                               <xsl:attribute name="left">
                                                                                  <xsl:value-of select="coordinates/left"/>
                                                                               </xsl:attribute>
                                                                               <xsl:attribute name="z">
                                                                                   <xsl:value-of select="coordinates/z"/>
                                                                               </xsl:attribute>
                                                                               <xsl:attribute name="top">
                                                                                   <xsl:value-of select="coordinates/top"/>
                                                                               </xsl:attribute>
                                                                     </xsl:element>
                                                                 </xsl:if>

                                                                 <xsl:if test="count(connections)!= 0">
                                                                        <xsl:element name="connections">
                                                                              <xsl:for-each select="connections/cout">
                                                                                 <xsl:element name="cout">
                                                                                        <xsl:if test="id != ''">
                                                                                           <xsl:attribute name="id">
                                                                                              <xsl:value-of select="id"/>
                                                                                           </xsl:attribute>
                                                                                         </xsl:if>
                                                                                 </xsl:element>
                                                                             </xsl:for-each>
                                                                             <xsl:for-each select="connections/cin">
                                                                                 <xsl:element name="cin">
                                                                                        <xsl:if test="id != ''">
                                                                                           <xsl:attribute name="id">
                                                                                              <xsl:value-of select="id"/>
                                                                                           </xsl:attribute>
                                                                                         </xsl:if>
                                                                                 </xsl:element>
                                                                             </xsl:for-each>
                                                                         </xsl:element>
                                                                 </xsl:if>
                                                                 </xsl:element>
                                                                </xsl:if>
                                                                  <xsl:if test="type = 'status'">
                                                                     <xsl:param name="actionstatustag"><xsl:value-of select="type"/></xsl:param>
                                                                     <xsl:element name="{$actionstatustag}">
                                                                             <xsl:attribute name="id"><xsl:value-of select="id" /></xsl:attribute>
                                                                             <xsl:attribute name="name"><xsl:value-of select="name" /></xsl:attribute>
                                                                              <xsl:attribute name="displayname"><xsl:value-of select="displayname"/></xsl:attribute>
                                                                          <xsl:element name="onsuccess">
                                                                            <xsl:attribute name="id"><xsl:value-of select="onsuccess/@id"/></xsl:attribute>
                                                                             <xsl:element name="map">
                                                                                  <xsl:attribute name="id"><xsl:value-of select="onsuccess/map/@id"/></xsl:attribute>
                                                                                  <xsl:attribute name="type"><xsl:value-of select="onsuccess/map/@type"/></xsl:attribute>
                                                                                  <xsl:element name="property">
                                                                                    <xsl:attribute name="name"><xsl:value-of select="onsuccess/map/property/@name"/></xsl:attribute>
                                                                                    <xsl:value-of select="onsuccess/map/property"/>
                                                                                  </xsl:element>
                                                                             </xsl:element>
                                                                          </xsl:element>
                                                                          <xsl:element name="onfailure">
                                                                            <xsl:attribute name="id"><xsl:value-of select="onfailure/@id"/></xsl:attribute>
                                                                             <xsl:element name="map">
                                                                                  <xsl:attribute name="id"><xsl:value-of select="onfailure/map/@id"/></xsl:attribute>
                                                                                  <xsl:attribute name="type"><xsl:value-of select="onfailure/map/@type"/></xsl:attribute>
                                                                                  <xsl:element name="property">
                                                                                     <xsl:attribute name="name"><xsl:value-of select="onfailure/map/property/@name"/></xsl:attribute>
                                                                                    <xsl:value-of select="onfailure/map/property"/>
                                                                                  </xsl:element>
                                                                             </xsl:element>
                                                                          </xsl:element>
                                                                         <xsl:if test="count(coordinates)!= 0">
                                                                             <xsl:element name="coordinates">
                                                                                       <xsl:attribute name="left">
                                                                                          <xsl:value-of select="coordinates/left"/>
                                                                                       </xsl:attribute>
                                                                                       <xsl:attribute name="z">
                                                                                           <xsl:value-of select="coordinates/z"/>
                                                                                       </xsl:attribute>
                                                                                       <xsl:attribute name="top">
                                                                                           <xsl:value-of select="coordinates/top"/>
                                                                                       </xsl:attribute>
                                                                             </xsl:element>
                                                                         </xsl:if>

                                                                         <xsl:if test="count(connections)!= 0">
                                                                                <xsl:element name="connections">
                                                                                      <xsl:for-each select="connections/cout">
                                                                                         <xsl:element name="cout">
                                                                                                <xsl:if test="id != ''">
                                                                                                   <xsl:attribute name="id">
                                                                                                      <xsl:value-of select="id"/>
                                                                                                   </xsl:attribute>
                                                                                                 </xsl:if>
                                                                                         </xsl:element>
                                                                                     </xsl:for-each>
                                                                                     <xsl:for-each select="connections/cin">
                                                                                         <xsl:element name="cin">
                                                                                                <xsl:if test="id != ''">
                                                                                                   <xsl:attribute name="id">
                                                                                                      <xsl:value-of select="id"/>
                                                                                                   </xsl:attribute>
                                                                                                 </xsl:if>
                                                                                         </xsl:element>
                                                                                     </xsl:for-each>
                                                                                 </xsl:element>
                                                                         </xsl:if>
                                                                      </xsl:element>
                                                                </xsl:if>
                                                        </xsl:if>
                                                    </xsl:for-each>
                                               </xsl:element>
                                            </xsl:for-each>
                                           </xsl:element>
                                       </xsl:element>
                                 </xsl:for-each>

                    </xsl:element>
                </xsl:for-each>
          </xsl:if>
          <!-- end of state element -->
           <xsl:element name="end-state">
                <xsl:attribute name="id">endid</xsl:attribute>
                <xsl:attribute name="name">end-state</xsl:attribute>
                <xsl:apply-templates select="end_state/connections"/>
                <xsl:apply-templates select="end_state/coordinates"/>
            </xsl:element>
        </process-definition>
         <!--end of root element template-->
    </xsl:template>
     <!--start_state/connections element template-->
     <xsl:template match="start_state/connections">
         <connections>
                    <xsl:for-each select="cout">
                           <xsl:element name="cout">
                                <xsl:if test="id !=''">
                                 <xsl:attribute name="id"> <xsl:value-of select="id"/></xsl:attribute>
                                 </xsl:if>
                            </xsl:element>
                    </xsl:for-each>
                     <xsl:for-each select="cin">
                            <xsl:element name="cin">
                                <xsl:if test="id !=''">
                                 <xsl:attribute name="id"> <xsl:value-of select="id"/></xsl:attribute>
                                 </xsl:if>
                            </xsl:element>
                     </xsl:for-each>
        </connections>
     </xsl:template>
      <!--end of start_state/connections element template-->
      <!--start_state/coordinates & connection element template-->
     <xsl:template match="start_state/coordinates">
        <xsl:element name="coordinates">
           <xsl:attribute name="left">
                <xsl:value-of select="left"/>
            </xsl:attribute>
             <xsl:attribute name="z">
                <xsl:value-of select="z"/>
            </xsl:attribute>
            <xsl:attribute name="top">
                <xsl:value-of select="top"/>
           </xsl:attribute>
       </xsl:element>
     </xsl:template>
     <xsl:template match="end_state/connections">
             <connections>
                      <xsl:for-each select="cout">
                           <xsl:element name="cout">
                                <xsl:if test="id !=''">
                                 <xsl:attribute name="id"> <xsl:value-of select="id"/></xsl:attribute>
                                 </xsl:if>
                            </xsl:element>
                      </xsl:for-each>
                     <xsl:for-each select="cin">
                            <xsl:element name="cin">
                                <xsl:if test="id !=''">
                                 <xsl:attribute name="id"> <xsl:value-of select="id"/></xsl:attribute>
                                 </xsl:if>
                            </xsl:element>
                     </xsl:for-each>
            </connections>
     </xsl:template>
      <!--end of start_state/coordinates & connection element template-->
       <!--end_state/coordinates element template-->
     <xsl:template match="end_state/coordinates">
        <xsl:element name="coordinates">
           <xsl:attribute name="left">
                <xsl:value-of select="left"/>
            </xsl:attribute>
            <xsl:attribute name="z">
                <xsl:value-of select="z"/>
            </xsl:attribute>
            <xsl:attribute name="top">
                <xsl:value-of select="top"/>
           </xsl:attribute>
       </xsl:element>
     </xsl:template>
      <!--end_state/coordinates element template-->
     <xsl:template match="state">
          <xsl:if test="count(./state) &gt; 0">
                <xsl:for-each select="./state">
                     <xsl:element name="state">
                                <xsl:attribute name="name">
                                    <xsl:value-of select="name"/>
                                </xsl:attribute>
                                <xsl:attribute name="isActive">
                                    <xsl:value-of select="isActive"/>
                                </xsl:attribute>
                                <xsl:attribute name="id">
                                    <xsl:value-of select="id"/>
                                </xsl:attribute>
                                <xsl:element name="event">
                                    <xsl:attribute name="type">node-enter</xsl:attribute>
                                    <xsl:apply-templates select="state/coordinates"/>
                               </xsl:element>
                    </xsl:element>
                </xsl:for-each>
          </xsl:if>
      </xsl:template>

      <xsl:template match="reader/coordinates">
        <xsl:element name="coordinates">
           <xsl:attribute name="left">
                <xsl:value-of select="reader/coordinates/left"/>
            </xsl:attribute>
            <xsl:attribute name="z">
                <xsl:value-of select="reader/coordinates/z"/>
            </xsl:attribute>
            <xsl:attribute name="top">
                <xsl:value-of select="reader/coordinates/top"/>
           </xsl:attribute>
       </xsl:element>
     </xsl:template>
</xsl:stylesheet>