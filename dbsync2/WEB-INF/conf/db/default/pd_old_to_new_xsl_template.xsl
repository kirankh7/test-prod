<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xml:output method ="xml" omit-xml-declaration="yes" indent="yes"/>
     <xsl:template match="process-definition">
         <!--root element template-->
        <process_definition>
             <xsl:attribute name="name">
                 <xsl:value-of select="@name"/>
             </xsl:attribute>
			 <xsl:attribute name="version">
               <xsl:text>2_1</xsl:text>
            </xsl:attribute>
           <xsl:element name="start_state">
                <xsl:attribute name="id">startid</xsl:attribute>
                <xsl:attribute name="name">start_state</xsl:attribute>
                <!--  <transition to="execute" />-->
                 <xsl:apply-templates select="start-state/transition"/>
                <xsl:apply-templates select="start-state/connections"/>
                <xsl:apply-templates select="start-state/coordinates"/>
           </xsl:element>

           <!-- state element templat.
           ***condtion to count all that have child elements / desendents in state element ***-->
            <xsl:if test="count(./state) &gt; 0">
                <xsl:for-each select="./state">
                     <xsl:element name="state">
                                <xsl:if test="@name !=''">
                                <xsl:attribute name="name">
                                    <xsl:value-of select="@name"/>
                                </xsl:attribute>
                                </xsl:if>
                                <xsl:if test="@isActive !=''">
                                <xsl:attribute name="isActive">
                                    <xsl:value-of select="@isActive"/>
                                </xsl:attribute>
                                </xsl:if>
                                 <xsl:if test="@id !=''">
                                <xsl:attribute name="id">
                                    <xsl:value-of select="@id"/>
                                </xsl:attribute>
                                </xsl:if>
                            <!--state connection element.loop start -->
                             <xsl:for-each select="connections">
                                <connections>
                                      <xsl:for-each select="cout">
                                           <xsl:element name="cout">
                                               <xsl:if test="@id != ''">
                                                 <xsl:attribute name="id"> <xsl:value-of select="@id"/></xsl:attribute>
                                                 </xsl:if>
                                            </xsl:element>
                                        </xsl:for-each>
                                         <xsl:for-each select="cin">
                                                <xsl:element name="cin">
                                                     <xsl:if test="@id != ''">
                                                     <xsl:attribute name="id"> <xsl:value-of select="@id"/></xsl:attribute>
                                                     </xsl:if>
                                                </xsl:element>
                                        </xsl:for-each>
                                </connections>
                            </xsl:for-each>
                             <!--state connection element. loop end -->
                              <!--state coordinates element. loop start -->
                            <xsl:for-each select="coordinates">
                                <xsl:element name="coordinates">
                                     <left>
                                        <xsl:value-of select="@left"/>
                                    </left>
                                   <z>
                                       <xsl:value-of select="@z"/>
                                   </z>
                                     <top>
                                         <xsl:value-of select="@top"/>
                                     </top>
                               </xsl:element>
                            </xsl:for-each>
                             <!--state coordinates element. loop end  -->
                                <!--child element event-->
                                 <xsl:for-each select="event">
                                        <xsl:element name="event">
                                            <xsl:attribute name="type">
                                                <xsl:if test="@type != ''">
                                                <xsl:value-of select="@type"/>
                                                </xsl:if>
                                            </xsl:attribute>
                                           <xsl:for-each select="action/reader">
                                                    <xsl:element name="action">
                                                        <xsl:if test="@id !=''">
                                                        <xsl:attribute name="id"><xsl:value-of select="@id" /></xsl:attribute>
                                                        <xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
                                                        <xsl:attribute name="displayname"><xsl:value-of select="@displayname" /></xsl:attribute>
                                                        <xsl:attribute name="type"><xsl:text>reader</xsl:text> </xsl:attribute>
                                                        </xsl:if>
                                                   <!--
                                                        <xsl:apply-templates select="state/event/action/coordinates"></xsl:apply-templates>
                                                       <xsl:apply-templates select="state/event/action/connections"></xsl:apply-templates>
                                                        <xsl:with-param name="color" select="state/event/action/connections"/>
                                                   -->
                                                   <xsl:if test="count(coordinates)!= 0">
                                                         <xsl:element name="coordinates">
                                                                   <xsl:attribute name="left">
                                                                      <xsl:value-of select="coordinates/@left"/>
                                                                   </xsl:attribute>
                                                                   <xsl:attribute name="z">
                                                                       <xsl:value-of select="coordinates/@z"/>
                                                                   </xsl:attribute>
                                                                   <xsl:attribute name="top">
                                                                       <xsl:value-of select="coordinates/@top"/>
                                                                   </xsl:attribute>
                                                         </xsl:element>
                                                     </xsl:if>

                                                      <xsl:if test="count(connections)!= 0">
                                                          <xsl:element name="connections">
                                                                  <xsl:for-each select="connections/cout">
                                                                     <xsl:element name="cout">
                                                                            <xsl:if test="@id != ''">
                                                                               <xsl:attribute name="id">
                                                                                  <xsl:value-of select="@id"/>
                                                                               </xsl:attribute>
                                                                             </xsl:if>
                                                                     </xsl:element>
                                                                 </xsl:for-each>
                                                                 <xsl:for-each select="connections/cin">
                                                                     <xsl:element name="cin">
                                                                            <xsl:if test="@id != ''">
                                                                               <xsl:attribute name="id">
                                                                                  <xsl:value-of select="@id"/>
                                                                               </xsl:attribute>
                                                                             </xsl:if>
                                                                     </xsl:element>
                                                                 </xsl:for-each>

                                                         </xsl:element>

                                                     </xsl:if>
                                                    </xsl:element>
                                            </xsl:for-each>
                                            <xsl:for-each select="action/task">
                                                    <xsl:for-each select="child::*">
                                                        <xsl:element name="action">
                                                              <xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute>
                                                              <xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
                                                               <xsl:attribute name="displayname"><xsl:value-of select="@displayname" /></xsl:attribute>
                                                              <xsl:attribute name="type"><xsl:value-of select="name()"/></xsl:attribute>
                                                              <xsl:param name="typename"><xsl:value-of select="name()"/></xsl:param>
                                                              <xsl:if test="$typename = 'map'">
                                                                      <xsl:attribute name="sequenceno"><xsl:value-of select="@sequenceno"/></xsl:attribute>
                                                               </xsl:if>
                                                               <xsl:if test="count(coordinates)!= 0">
                                                                 <xsl:element name="coordinates">
                                                                           <xsl:attribute name="left">
                                                                              <xsl:value-of select="coordinates/@left"/>
                                                                           </xsl:attribute>
                                                                           <xsl:attribute name="z">
                                                                               <xsl:value-of select="coordinates/@z"/>
                                                                           </xsl:attribute>
                                                                           <xsl:attribute name="top">
                                                                               <xsl:value-of select="coordinates/@top"/>
                                                                           </xsl:attribute>
                                                                 </xsl:element>
                                                             </xsl:if>
                                                              <xsl:if test="count(connections)!= 0">
                                                                  <xsl:element name="connections">
                                                                          <xsl:for-each select="connections/cout">
                                                                             <xsl:element name="cout">
                                                                                    <xsl:if test="@id != ''">
                                                                                       <xsl:attribute name="id">
                                                                                          <xsl:value-of select="@id"/>
                                                                                       </xsl:attribute>
                                                                                     </xsl:if>
                                                                             </xsl:element>
                                                                         </xsl:for-each>
                                                                         <xsl:for-each select="connections/cin">
                                                                             <xsl:element name="cin">
                                                                                    <xsl:if test="@id != ''">
                                                                                       <xsl:attribute name="id">
                                                                                          <xsl:value-of select="@id"/>
                                                                                       </xsl:attribute>
                                                                                     </xsl:if>
                                                                             </xsl:element>
                                                                         </xsl:for-each>
                                                                 </xsl:element>
                                                             </xsl:if>
                                                        </xsl:element>
                                                    </xsl:for-each>
                                            </xsl:for-each>
                                       </xsl:element>
                                 </xsl:for-each>
                    </xsl:element>
                </xsl:for-each>
          </xsl:if>
          <!-- end of state element -->
           <xsl:element name="end_state">
                <xsl:attribute name="id">endid</xsl:attribute>
                <xsl:attribute name="name">end_state</xsl:attribute>
                <xsl:apply-templates select="end-state/connections"/>
                <xsl:apply-templates select="end-state/coordinates"/>
            </xsl:element>
        </process_definition>
         <!--end of root element template-->
    </xsl:template>
      <!--start-state/transition element template-->
    <xsl:template match="start-state/transition">
         <transition>
                    <xsl:attribute name="to"><xsl:value-of select="@to"/></xsl:attribute>
        </transition>
     </xsl:template>
     <!--start_state/connections element template-->
     <xsl:template match="start-state/connections">
         <connections>
                    <xsl:for-each select="cout">
                           <xsl:element name="cout">
                                <xsl:if test="@id !=''">
                                 <xsl:attribute name="id"> <xsl:value-of select="@id"/></xsl:attribute>
                                 </xsl:if>
                            </xsl:element>
                    </xsl:for-each>
                     <xsl:for-each select="cin">
                            <xsl:element name="cin">
                                <xsl:if test="@id !=''">
                                 <xsl:attribute name="id"> <xsl:value-of select="@id"/></xsl:attribute>
                                 </xsl:if>
                            </xsl:element>
                     </xsl:for-each>
        </connections>
     </xsl:template>
      <!--end of start_state/connections element template-->
      <!--start_state/coordinates & connection element template-->
     <xsl:template match="start-state/coordinates">
        <xsl:element name="coordinates">
           <left>
                <xsl:value-of select="@left"/>
            </left>
           <z>
               <xsl:value-of select="@z"/>
           </z>
             <top>
                 <xsl:value-of select="@top"/>
             </top>
       </xsl:element>
     </xsl:template>
     <xsl:template match="end-state/connections">
             <connections>
                       <xsl:for-each select="cout">
                           <xsl:element name="cout">
                                <xsl:if test="@id !=''">
                                 <xsl:attribute name="id"> <xsl:value-of select="@id"/></xsl:attribute>
                                 </xsl:if>
                            </xsl:element>
                    </xsl:for-each>
                     <xsl:for-each select="cin">
                            <xsl:element name="cin">
                                <xsl:if test="@id !=''">
                                 <xsl:attribute name="id"> <xsl:value-of select="@id"/></xsl:attribute>
                                 </xsl:if>
                            </xsl:element>
                     </xsl:for-each>
            </connections>
     </xsl:template>
      <!--end of start_state/coordinates & connection element template-->
       <!--end_state/coordinates element template-->
     <xsl:template match="end-state/coordinates">
        <xsl:element name="coordinates">
            <left>
                <xsl:value-of select="@left"/>
            </left>
           <z>
               <xsl:value-of select="@z"/>
           </z>
             <top>
                 <xsl:value-of select="@top"/>
             </top>
       </xsl:element>
     </xsl:template>
      <!--end_state/coordinates element template-->
     <xsl:template match="state">
          <xsl:if test="count(./state) &gt; 0">
                <xsl:for-each select="./state">
                     <xsl:element name="state">
                                <xsl:attribute name="name">
                                    <xsl:value-of select="@name"/>
                                </xsl:attribute>
                                <xsl:attribute name="isActive">
                                    <xsl:value-of select="@isActive"/>
                                </xsl:attribute>
                                <xsl:attribute name="id">
                                    <xsl:value-of select="@id"/>
                                </xsl:attribute>
                                <xsl:element name="event">
                                    <xsl:attribute name="type">node_enter</xsl:attribute>
                                    <xsl:apply-templates select="state/coordinates"/>
                               </xsl:element>
                    </xsl:element>
                </xsl:for-each>
          </xsl:if>
      </xsl:template>
      <xsl:template match="state/event/action/coordinates">
        <xsl:element name="coordinates">
            <left>
                <xsl:value-of select="@left"/>
            </left>
           <z>
               <xsl:value-of select="@z"/>
           </z>
             <top>
                 <xsl:value-of select="@top"/>
             </top>
       </xsl:element>
     </xsl:template>
</xsl:stylesheet>
