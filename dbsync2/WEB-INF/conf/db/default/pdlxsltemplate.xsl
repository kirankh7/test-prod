<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- Edited by XMLSpy� -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="process-definition">
    <process_definition>
        <name>
            <xsl:value-of select="@name"/>
        </name>
        <start_state name="start_state" id="startid">
            <connections>
              <cout>
                <id>endid</id>
              </cout>
              <cout/>
              <cin />
              <cin/>
            </connections>
        <coordinates>
          <left>50</left>
          <z>99</z>
          <top>50</top>
        </coordinates>
        </start_state>
        <state/>
        <state/>
        <end_state name="end_state" id="endid">
            <connections>
              <cout />
              <cout/>
              <cin>
                <id>startid</id>
              </cin>
              <cin/>
            </connections>
            <coordinates>
              <left>200</left>
              <z>99</z>
              <top>200</top>
            </coordinates>
       </end_state>
    </process_definition>
    </xsl:template>
</xsl:stylesheet>