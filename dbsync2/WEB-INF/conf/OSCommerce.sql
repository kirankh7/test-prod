/*		DDL for creating sync columns in OSCommerce */

/***********************************************************************************
							CUSTOMERS
************************************************************************************/

delimiter //

alter table customers add column sync_status smallint default 1//
alter table customers add column sync_externid varchar(30) unique not null//

drop trigger update_customers_set_sync_status//

create trigger update_customers_set_sync_status before update on customers
for each row begin
	if new.sync_status = 0 and old.sync_status = 0 then
		set new.sync_status = 1;
	end if;
end;//

drop trigger update_address_book_set_sync_status//

create trigger update_address_book_set_sync_status before update on address_book
for each row begin
	update customers set sync_status = 1 where customers_id = new.customers_id;
end;//

drop trigger insert_address_book_set_sync_status//

create trigger insert_address_book_set_sync_status before insert on address_book
for each row begin
	update customers set sync_status = 1 where customers_id = new.customers_id;
end;//

delimiter ;

/***********************************************************************************
							ORDERS
************************************************************************************/

delimiter //

alter table orders add column sync_status smallint default 1//
alter table orders add column sync_externid varchar(30) unique not null//

drop trigger orders_set_sync_status//

create trigger orders_set_sync_status before update on orders
for each row begin
	if new.sync_status = 0 and old.sync_status = 0 then
		set new.sync_status = 1;
	end if;
end;//

drop trigger update_orders_total_set_sync_status//

create trigger update_orders_total_set_sync_status before update on orders_total
for each row begin
	update orders set sync_status = 1 where orders_id = new.orders_id;
end;//

drop trigger insert_orders_total_set_sync_status//

create trigger insert_orders_total_set_sync_status before insert on orders_total
for each row begin
	update orders set sync_status = 1 where orders_id = new.orders_id;
end;//

drop trigger update_orders_products_set_sync_status//

create trigger update_orders_products_set_sync_status before update on orders_products
for each row begin
	update orders set sync_status = 1 where orders_id = new.orders_id;
end;//

drop trigger insert_orders_products_set_sync_status//

create trigger insert_orders_products_set_sync_status before insert on orders_products
for each row begin
	update orders set sync_status = 1 where orders_id = new.orders_id;
end;//

/***********************************************************************************
							PRODUCTS
************************************************************************************/

delimiter //

alter table products add column external_id varchar(100) unique not null//

alter table products_description modify products_id int not null;

insert into categories (categories_id, parent_id, sort_order, date_added, last_modified) values (100000, 0, 4, now(), now());
insert into categories_description values (100000, 1, 'Imported');

drop trigger before_insert_products//

create trigger before_insert_products before insert on products
for each row begin
	set new.products_date_added = now();
	set new.products_date_available = now();
end;//

drop trigger before_update_products//

create trigger before_update_products before update on products
for each row begin
	set new.products_last_modified = now();
end;//


delimiter ;

