var Avankia = function(){}

Avankia.isArray = function(obj)
{
   if (obj.constructor.toString().indexOf("Array") == -1)
      return false;
   else
      return true;
}

Avankia.selectSingleNode = function(node, path)
{
	var doc = node.ownerDocument;

	// the following two lines are needed for IE
	doc.setProperty("SelectionNamespaces", "xmlns:xsl='http://www.w3.org/1999/XSL/Transform'");
	doc.setProperty("SelectionLanguage", "XPath");
	return node.selectSingleNode(path);
}

Avankia.selectNodes = function(node, path)
{
	var doc = node.ownerDocument;
alert(doc);
	// the following two lines are needed for IE
	doc.setProperty("SelectionNamespaces", "xmlns:xsl='http://www.w3.org/1999/XSL/Transform'");
	doc.setProperty("SelectionLanguage", "XPath");
alert(node.selectNodes);
	var ret = node.selectNodes(path);
alert(ret);
	return ret;
}

Avankia.createCallback = function(successFunction)
{
	return callback =
	{
		customevents:{
			onStart:function(eventType, args) {
			  // eventType has a string value of "startEvent".
			  // args[0].tId is the integer transaction ID.
			  // args[1] contains the value of <code>callback.argument</code>, if callback.argument is defined.
			},
			onComplete:function(eventType, args) {
			  // eventType has a string value of "completeEvent".
			  // args[0].tId is the integer transaction ID.
			  // args[1] contains the value of <code>callback.argument</code>, if callback.argument is defined.
			},

			onSuccess:successFunction,

			onFailure:function(eventType, args) {
			  // eventType has a string value of "failureEvent".
			  // args[0] is the response object.
			   alert("failed: " + args[0]);
			},

	        // Define this event handler for file upload transactions *only*.
	        // This handler will not be used for any other transaction cases.
			onUpload:function(eventType, args) {
			  // eventType has a string value of "uploadEvent".
			  // args[0] is the response object.
			},
			onAbort:function(eventType, args) {
			  // eventType has a string value of "abortEvent".
			  // args[0].tId is the integer transaction ID.
			  // args[1] contains the value of <code>callback.argument</code>, if callback.argument is defined.
			}
		}
	}
}

Avankia.callDBSyncXML = function(methodString, payload, callback)
{
	payload = JSON.stringify(payload);

	if (payload[0] == "\"" && payload[payload.length - 1] == "\"")
		payload = payload.substring(1, payload.length - 1);

	var authenticationXML = "<authentication>" + "<userName>jcrater@nctc.com</userName>" + "<password>password</password>" + "<profile>nctc</profile>" + "</authentication>";
	var params = "authenticationXML=" + authenticationXML + "&" + "methodNameString=" + methodString + "&" + "payload=" + payload;

	var _dbsyncCallback = createCallback
	(
		function(eventType, args)
		{
			var responseXML = args[0].responseXML;
			responseXML.setProperty("SelectionNamespaces", "xmlns:xsl='http://www.w3.org/1999/XSL/Transform'");
			responseXML.setProperty("SelectionLanguage", "XPath");
			var results = responseXML.selectSingleNode("response/results");
			callback(results);
		}
	);

	YAHOO.util.Connect.asyncRequest( "POST", "dbsyncrequestrestservice.m", _dbsyncCallback, params);
}

Avankia.callDBSync = function(methodString, payload, callback)
{
	payload = JSON.stringify(payload);

	if (payload[0] == "\"" && payload[payload.length - 1] == "\"")
		payload = payload.substring(1, payload.length - 1);

	var authenticationXML = "<authentication>" + "<userName>jcrater@nctc.com</userName>" + "<password>password</password>" + "<profile>nctc</profile>" + "</authentication>";
	var params = "authenticationXML=" + authenticationXML + "&" + "methodNameString=" + methodString + "&" + "payload=" + payload;

	var _dbsyncCallback = createCallback
	(
		function(eventType, args)
		{
			var responseXML = args[0].responseXML;
			// the following two lines are needed for IE
			responseXML.setProperty("SelectionNamespaces", "xmlns:xsl='http://www.w3.org/1999/XSL/Transform'");
			responseXML.setProperty("SelectionLanguage", "XPath");
			var resultsJSON = XPathSelectSingleNode(responseXML, "response/results/text()").data;
			var data = eval("(" + resultsJSON + ")");
			callback(data);
		}
	);

	YAHOO.util.Connect.asyncRequest( "POST", "dbsyncrequestrestservice.m", _dbsyncCallback, params);
}

