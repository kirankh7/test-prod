call "setenv.bat"

set CONFIG_DIR=%SYNCH_HOME%/dbsync/WEB-INF/conf

set JAVA_HOME=%SYNCH_HOME%/jre1.5.0_11

set CR=%SYNCH_HOME%\dbsync\WEB-INF\lib

set CLASSPATH=%SYNCH_HOME%\dbsync\WEB-INF\classes;%CR%\axis.jar;%CR%\axis-ant.jar;%CR%\commons-discovery-0.2.jar;%CR%\commons-logging-1.0.4.jar;%CR%\jaxrpc.jar;%CR%\log4j-1.2.8.jar;%CR%\saaj.jar;%CR%\wsdl4j-1.5.1.jar;%CR%\ojdbc14.jar;%CR%\avankia_framework.jar;%CR%\mail.jar;%CR%\activation.jar;%CR%\commons-digester.jar;%CR%\commons-collections-3.1.jar;%CR%\commons-beanutils.jar;%CR%\commons-dbcp-1.2.1.jar;%CR%\commons-pool-1.1.jar;%CR%\ojdbc14.jar;%CR%\sqljdbc.jar;%CR%\mysql-connector-java-3.1.13-bin.jar;%CR%\concurrent-1.3.4.jar;%CR%\commons-httpclient-2.0.jar;%CR%\jdom.jar;%CR%\ostermillerutils_1_05_00_for_java_1_4.jar


java -Xmx512m -Dinstance=%1 -Dscheduler.delay=%2 -Dconfig.dir=%CONFIG_DIR% -classpath %CLASSPATH% com.avankia.salesforce.dbsynch.TimerLocalSyncDriver %3 %4

